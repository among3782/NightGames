package status;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;

public class Beastform extends Status {

	public Beastform(Character affected) {
		super("Beastform",affected);
		this.traits.add(Trait.tailed);
		this.flags.add(Stsflag.beastform);
		tooltip = "25% Power and Speed bonus. Doubled if also Feral";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			duration = 15;
		}else{
			this.duration = 10;
		}
		this.affected = affected;
		
	}
	
	@Override
	public int mod(Attribute a) {
		if(affected.is(Stsflag.feral)){
			switch(a){
			case Power:
				return affected.getPure(Attribute.Power)/2;
			case Speed:
				return affected.getPure(Attribute.Speed)/2;
			case Animism:
				return 10;
			}
		}else{
			switch(a){
			case Power:
				return affected.getPure(Attribute.Power)/4;
			case Speed:
				return affected.getPure(Attribute.Speed)/4;
			case Animism:
				return 10;
			}
		}
		return 0;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your animal transformation is still stable, giving you enhanced physical capability.";
		}
		else{
			return affected.name()+" is partially transformed into a feral animal.";
		}
	}

	@Override
	public Status copy(Character target) {
		return new Beastform(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.dominant,5);
		decay(c);
	}
}
