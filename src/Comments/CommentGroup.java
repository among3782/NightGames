package Comments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class CommentGroup {
	private HashMap<CommentTrigger,ArrayList<String>> comments;
	
	public CommentGroup(){
		comments = new HashMap<CommentTrigger,ArrayList<String>>();
	}
	
	public void put(CommentTrigger situation, String comment){
		if(!comments.containsKey(situation)){
		    comments.put(situation, new ArrayList<String>());
        }
		comments.get(situation).add(comment);
	}

	public Set<CommentTrigger> getTriggers(){
	    return comments.keySet();
    }
	
	public ArrayList<String> getComments(CommentTrigger situation){
		return comments.get(situation);
	}
}
