package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Stsflag;

public class Ravage extends Skill {
    public Ravage(Character self) {
        super("Ravage", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Eldritch) >= 30;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return !self.stunned() && !self.distracted() && !self.is(Stsflag.enthralled) && target.getStatusMagnitude("Entangled") >= 100;
    }

    @Override
    public String describe() {
        return null;
    }

    @Override
    public void resolve(Combat c, Character target) {

    }

    @Override
    public Skill copy(Character user) {
        return null;
    }

    @Override
    public Tactics type() {
        return null;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return null;
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return null;
    }
}
