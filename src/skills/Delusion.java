package skills;

import characters.*;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.ProfMod;
import status.ProfessionalMomentum;

public class Delusion extends Skill{

    public Delusion(Character user){
        super("Delusion",user);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Unknowable)>=6;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canSpend(Pool.ENIGMA,2)&&self.canAct()&&c.stance.penetration(self);
    }

    @Override
    public String describe() {
        return "";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.ENIGMA,2);
        if(target.get(Attribute.Spirituality)>=2){
            if(self.human()){
                c.write(self,deal(0, Result.miss,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.miss,target));
            }
        }else if(c.isWatching(ID.ANGEL)){
            if(self.human()){
                c.write(self,deal(0, Result.defended,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.defended,target));
            }
        }else{
            int m = self.getSexPleasure(3, Attribute.Seduction)+target.get(Attribute.Perception)+self.get(Attribute.Unknowable);
            m += self.getMojo().get()/5;

            if(self.human()){
                c.write(self,deal(0, Result.normal,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.normal,target));
            }

            target.pleasure(m,Anatomy.genitals, Result.finisher,c);
            int r = target.getSexPleasure(3, Attribute.Seduction);
            r += target.bonusRecoilPleasure(r);
            if(self.has(Trait.experienced)){
                r = r/2;
            }
            self.pleasure(r,Anatomy.genitals, Result.finisher,c);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new Delusion(user);
    }

    @Override
    public Tactics type() {
        return Tactics.fucking;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "";
        }else if(modifier == Result.defended){
            return "";
        }else{
            return "";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "";
        }else if(modifier == Result.defended){
            return "[Placeholder: Angel Assists]";
        }else{
            return "";
        }
    }
}
