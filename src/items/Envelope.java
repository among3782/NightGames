package items;

import java.util.ArrayList;

import characters.Character;
import global.Challenge;

public class Envelope implements Item {
	private Challenge goal;
	public Envelope(Challenge goal){
		this.goal = goal;
	}
	@Override
	public String getDesc() {
		return goal.message();
	}
	public String getFullDesc(Character owner) {
		return getDesc();
	}

	@Override
	public int getPrice() {
		return 0;
	}

	@Override
	public String getName() {
		return "Challenge: "+goal.summary();
	}
	public String getFullName(Character owner){
		return getName();
	}

	@Override
	public String pre() {
		// TODO Auto-generated method stub
		return "a ";
	}
	@Override
	public Boolean listed() {
		return true;
	}
	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<Item>();
	}

	@Override
	public void pickup(Character owner) {
		// TODO Auto-generated method stub
		
	}

}
