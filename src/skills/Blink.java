package skills;

import characters.*;
import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Pin;

public class Blink extends Skill{

    public Blink(Character user){
        super("Blink",user);
    }
    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Unknowable)>=18;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canAct() && self.canSpend(Pool.ENIGMA,2);
    }

    @Override
    public String describe() {
        return "Teleport on top of your opponent to pin them";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.ENIGMA,4);
        if(target.get(Attribute.Ninjutsu)>=10) {
            if (self.human()) {
                c.write(self, deal(0, Result.miss, target));
            } else if (target.human()) {
                c.write(self, receive(0, Result.miss, target));
            }
        }else if(target.get(Attribute.Animism)>=10){
                if(self.human()){
                    c.write(self,deal(1, Result.miss,target));
                }
                else if(target.human()){
                    c.write(self,receive(1, Result.miss,target));
                }
        }else if(c.isWatching(ID.YUI)){
            if(self.human()){
                c.write(self,deal(0, Result.defended,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.defended,target));
            }
        }else if(c.isWatching(ID.KAT)){
            if(self.human()){
                c.write(self,deal(1, Result.defended,target));
            }
            else if(target.human()){
                c.write(self,receive(1, Result.defended,target));
            }
        }else{
            if(self.human()){
                c.write(self,deal(0, Result.normal,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.normal,target));

            }
            c.stance = new Pin(self,target);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new Blink(user);
    }

    @Override
    public Tactics type() {
        return Tactics.positioning;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            if(damage ==1){
                return "";
            }else{
                return "";
            }
        }else if(modifier == Result.defended){
            if(damage ==1){
                return "[Placeholder: Kat Assists]";
            }else{
                return "[Placeholder: Yui Assists]";
            }
        }else{
            return "";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            if(damage ==1){
                return self.name()+" suddenly disappears from your vision. Before you can even think to react, you feel the " +
                        "sudden existence of a woman-sized, woman-shaped object on top of you. It turns out to be "+self.name()+
                        " which is physically impossible, but still makes more sense than if it was anyone else. Fortunately, your " +
                        "animal instincts are quicker than your brain, kicking up with both feet to push "+self.pronounTarget(false)+" off.";
            }else{
                return self.name()+" suddenly disappears from your vision. Before you can even think to react, you feel the " +
                        "sudden existence of a woman-sized, woman-shaped object on top of you. It turns out to be "+self.name()+
                        " which is physically impossible, but still makes more sense than if it was anyone else. You have just enough " +
                        "time to drop a smoke bomb and dodge out of the way before "+self.pronounSubject(false)+" can grab you.";
            }
        }else if(modifier == Result.defended){
            if(damage ==1){
                return "[Placeholder: Kat Assists]";
            }else{
                return "[Placeholder: Yui Assists]";
            }
        }else{
            return self.name()+" suddenly disappears from your vision. Before you can even think to react, you feel the " +
                    "sudden existence of a woman-sized, woman-shaped object on top of you. It turns out to be "+self.name()+
                    " which is physically impossible, but still makes more sense than if it was anyone else. However, this realization " +
                    "comes too late to prevent you from being pinned on the floor.";
        }
    }
}
