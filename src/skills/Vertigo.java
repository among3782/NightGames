package skills;

import characters.*;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.StandingOver;

public class Vertigo extends Skill{
    public Vertigo(Character self) {
        super("Vertigo", self);
        addTag(Attribute.Unknowable);
        addTag(SkillTag.PAINFUL);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Unknowable)>=3;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c) && !c.stance.prone(target) && self.canSpend(Pool.ENIGMA,2);
    }

    @Override
    public String describe() {
        return "Incapacitate your opponent and deliver a coup de grace: 2 Enigma";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.ENIGMA,2);
        if(target.get(Attribute.Vigilantism)>=2){
            if(self.human()){
                c.write(self,deal(0, Result.miss,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.miss,target));
            }
        }else if(c.isWatching(ID.JEWEL)){
            if(self.human()){
                c.write(self,deal(0, Result.defended,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.defended,target));
            }
        }else{
            c.stance = new StandingOver(self,target);
            int pow = 30 + self.get(Attribute.Power);

            if(self.human()){
                c.write(self,deal(0, Result.normal,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.normal,target));
                if(Global.random(5)>=1){
                    c.write(self,self.bbLiner());
                }
            }
            target.pain(pow-(Global.random(2)*target.bottom.size()), Anatomy.genitals,c);
            target.calm(Global.random(pow),c);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new Vertigo(user);
    }

    @Override
    public Tactics type() {
        return Tactics.damage;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "";
        }else if(modifier == Result.defended){
            return "";
        }else{
            return "";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "The world suddenly seems to warp around you until you can't tell up from down. As you come to your senses, " +
                    "you realize you are lying on the floor and "+self.name()+" is standing over you with one foot raised. With almost " +
                    "supernatural reflexes, you launch yourself out of the way before her foot can come down.";
        }else if(modifier == Result.defended){
            return "[Placeholder: Jewel Assists]";
        }else{
            return "The world suddenly seems to warp around you until you can't tell up from down. As you come to your senses, " +
                    "you realize you are lying on the floor and "+self.name()+" is standing over you with one foot raised. Before you " +
                    "can react, she stomps down on your groin.";
        }
    }
}
