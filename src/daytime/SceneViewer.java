package daytime;

import characters.ID;
import global.Global;
import characters.Character;
import global.Roster;
import scenes.Scene;
import scenes.SceneFlag;
import scenes.SceneManager;
import scenes.SceneType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SceneViewer extends Activity{
    private Activity parent;
    private ID selected;
    private ID target;
    private SceneType category;
    private List<SceneFlag> watched;
    private HashSet<ID> selectable;
    private HashSet<SceneType> categories;
    private ArrayList<SceneFlag> availableScenes;

    @Override
    public boolean known() {
        return false;
    }

    public SceneViewer(Character player, Activity parent){
        super("Scene Viewer",player);
        this.parent = parent;
        watched = Global.getAllWatched();
        selectable = new HashSet<ID>();
        categories = new HashSet<SceneType>();
        availableScenes = new ArrayList<SceneFlag>();
    }

    @Override
    public void visit(String choice) {
        Global.gui().clearText();
        Global.gui().clearCommand();
        if (choice.equalsIgnoreCase("Start") || choice.equalsIgnoreCase( "Back")) {
            Global.gui().message("You review the match footage you were sent. You can revist some of the highlights of your sexual escapades here.");
            selected = null;
            category = null;
            target = null;
            selectable.clear();
            availableScenes.clear();
            categories.clear();
            for (SceneFlag f: watched){
                selectable.add(f.getStar());
            }
            for(ID actor: selectable){
                Global.gui().choose(this, Roster.get(actor).name());
            }
            Global.gui().choose(parent,"Back");
        }else if(selected==null){
            selected = ID.fromString(choice);
            categories.clear();
            int count = 0;
            for(SceneFlag f: watched){
                if(f.getStar()==selected){
                    count++;
                    categories.add(f.getType());
                }
            }
            for (SceneType type: categories){
                Global.gui().choose(this, type.toString());
            }
            Global.gui().choose(this, "Back");
            Global.gui().message("Scenes featuring: "+choice);
            Global.gui().message(count+" total scenes available.");

        }else if(category== null){
            category = SceneType.valueOf(choice);
            availableScenes.clear();
            for(SceneFlag f: watched){
                if(f.getStar()==selected && f.getType()==category){
                    availableScenes.add(f);
                }
            }
            Global.gui().message(category.toString()+" scenes featuring "+Roster.get(selected).name()+".");
            Global.gui().message(availableScenes.size()+" of "+SceneManager.getTotalCount(selected,category)+" seen.");
            if(category == SceneType.INTERVENTION ){
                Global.gui().message("Choose the other person to appear in the scene.");
                for(Character npc: Roster.getCombatants()){
                    if(npc.id()!=selected && npc.id()!=ID.PLAYER){
                        Global.gui().choose(this,npc.name());
                    }
                }
            }else{
                for(SceneFlag f: availableScenes){
                    Global.gui().choose(this,f.getLabel());
                }
            }
            Global.gui().choose(this, "Back");
        }else if(category==SceneType.INTERVENTION && target == null){
            target = ID.fromString(choice);
            availableScenes.clear();
            for(SceneFlag f: watched){
                if(f.getStar()==selected && f.getType()==category){
                    availableScenes.add(f);
                }
            }
            Global.gui().message(category.toString()+" scenes featuring "+Roster.get(selected).name()+".");
            Global.gui().message(availableScenes.size()+" of "+SceneManager.getTotalCount(selected,category)+" seen.");
            Global.gui().message("Additional appearance by: "+Roster.get(target).name()+".");
            for(SceneFlag f: availableScenes){
                Global.gui().choose(this,f.getLabel());
            }
            Global.gui().choose(this, "Back");
        }else{
            for(SceneFlag f: availableScenes){
                if(choice.equalsIgnoreCase(f.getLabel())){
                    SceneManager.play(f,Roster.get(target));
                }
            }
            Global.gui().choose(this, "Back");
        }
    }

    @Override
    public void shop(Character npc, int budget) {

    }
}
