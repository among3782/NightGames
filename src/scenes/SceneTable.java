package scenes;

public abstract class SceneTable {
    public Scene getScene(SceneFlag flag){
        switch (flag.getType()){
            case VICTORY:
                return getVictoryScene(flag);
            case DEFEAT:
                return getDefeatScene(flag);
            case DRAW:
                return getDrawScene(flag);
            case INTERVENTION:
                return getInterventionScene(flag);
            case EVENT:
                return getEventScene(flag);
            case DAYTIME:
                return getDayScene(flag);
        }
        return new Scene("Error! Scene "+flag.getLabel()+" not found.");
    }

    public abstract Scene getVictoryScene(SceneFlag flag);
    public abstract Scene getDefeatScene(SceneFlag flag);
    public abstract Scene getDrawScene(SceneFlag flag);
    public abstract Scene getInterventionScene(SceneFlag flag);
    public abstract Scene getEventScene(SceneFlag flag);
    public abstract Scene getDayScene(SceneFlag flag);
}
