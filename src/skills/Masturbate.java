package skills;

import characters.*;
import characters.Character;
import combat.Result;
import global.Modifier;

import combat.Combat;
import global.Scheduler;

public class Masturbate extends Skill {

	public Masturbate(Character self) {
		super("Masturbate", self);
		addTag(SkillTag.MASTURBATION);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			if(self.getArousal().get()<=15){
				c.write(self,deal(0, Result.weak,target));
			}
			else{
				c.write(self,deal(0, Result.normal,target));
				c.offerImage("Masturbation.jpg", "Art by AimlessArt");
			}
		}
		else if(target.human()){
			c.write(self,receive(0, Result.normal,target));
		}
		self.pleasure(20,Anatomy.genitals,c);
		if(self.has(Trait.showoff)){
			self.buildMojo(80);
		}else{
			self.buildMojo(20);
		}
		self.emote(Emotion.horny, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new Masturbate(user);
	}

	@Override
	public Tactics type() {
		return Tactics.misc;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.weak){
			return "You take hold of your flaccid dick, tugging and rubbing it into a full erection.";
		}
		else{
			return "You jerk off, building up your own arousal.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" slips her hand between her legs and starts fingering herself.";
	}

	@Override
	public String describe() {
		return "Raises your own Arousal and boosts your Mojo";
	}

}
