package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Stance;
import status.Flatfooted;
import status.Stsflag;

public class Feint extends Skill {


    public Feint(Character self){
        super("Charge Feint", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 1;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c) && c.stance.en== Stance.neutral;
    }

    @Override
    public String describe() {
        return "Fake out your opponent with a lunge.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        if(c.effectRoll(this, self, target, self.get(Attribute.Speed)+self.get(Attribute.Footballer)/2)&&!target.is(Stsflag.cynical)){
            if(self.human()){
                c.write(self,deal(0,Result.normal,target));
            }
            else{
                c.write(self,receive(0,Result.normal,target));
            }
            target.add(new Flatfooted(target,1),c);
        }else{
            if(self.human()){
                c.write(self,deal(0,Result.miss,target));
            }
            else{
                c.write(self,receive(0,Result.miss,target));
            }
        }

    }

    @Override
    public int accuracy(){
        return self.get(Attribute.Cunning);
    }

    @Override
    public Skill copy(Character user) {
        return new Feint(user);
    }

    @Override
    public Tactics type() {
        return Tactics.status;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return null;
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return null;
    }
}
