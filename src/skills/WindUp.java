package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Primed;

public class WindUp extends Skill {

	public WindUp(Character self) {
		super("Wind-up", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal)>=1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&!c.stance.prone(self)&&self.canAct()&&!self.getPool(Pool.TIME).isFull();
	}

	@Override
	public String describe() {
		return "Primes time charges: first charge free, 10 Mojo for each additional charge";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int charges = Math.min(4, self.getMojo().get()/10);
		if(charges + self.getPool(Pool.TIME).get()>self.getPool(Pool.TIME).max()){
			charges = self.getPool(Pool.TIME).max() - self.getPool(Pool.TIME).get();
		}
		
		self.spendMojo(charges*10);
		if(self.human()){
			c.write(self,deal(charges, Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(charges, Result.normal,target));
		}
		self.getPool(Pool.TIME).restore(charges);
	}

	@Override
	public Skill copy(Character user) {
		return new WindUp(user);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You take advantage of a brief lull in the fight to wind up your Procrastinator, priming "+damage+" time charges for later use.");
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s fiddles with a small device on %s wrist.",self.name(),self.possessive(false));
	}

}
