package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Status;

import java.util.ArrayList;

public class Purge extends Skill{

    public Purge(Character self){
        super("Purge",self);
        addTag(Attribute.Spirituality);
    }
    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Spirituality)>=6;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canSpend(Pool.FOCUS,2) && self.canActNormally(c) && !target.getStatus().isEmpty();

    }

    @Override
    public String describe() {
        return "Convert your opponent's abnormal statuses into temptation.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.FOCUS,2);

        int count = target.getStatus().size();
        target.tempt(20*count,c);
        if(self.human()){
            c.write(self,deal(0, Result.normal,target));
        }
        else if(target.human()){
            c.write(self,receive(0, Result.normal,target));
        }
        ArrayList<Status> copy = new ArrayList<Status>();
        copy.addAll(target.getStatus());
        for(Status s: copy){
            target.removeStatus(s,c);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new Purge(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "You use a spiritual cleansing ritual to purge all the abnormalities from "+target.name()+". " +
                "As a pleasant side-effect, it should be highly arousing.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return self.name()+" waves her staff at you. You feel... clean... like everything has been forcefully purged from your spirit. " +
                "For whatever reason, you also feel weirdly turned on.";
    }
}
