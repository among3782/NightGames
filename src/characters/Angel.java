package characters;

import Comments.SkillComment;
import combat.Result;
import daytime.Daytime;
import global.*;

import items.Clothing;
import items.Toy;
import items.Trophy;

import java.util.ArrayList;
import java.util.HashSet;

import Comments.CommentGroup;
import Comments.CommentSituation;
import pet.Ptype;
import scenes.SceneFlag;
import scenes.SceneManager;
import skills.*;
import stance.Stance;
import status.Drowsy;
import status.Energized;
import combat.Combat;
import combat.Tag;

import actions.Action;
import actions.Movement;
import status.Stsflag;

public class Angel implements Personality {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8169646189131720872L;
	public NPC character;
	
	public Angel(){
		character = new NPC("Angel", ID.ANGEL, 1,this);
		character.outfit[0].add(Clothing.Tshirt);
		character.outfit[1].add(Clothing.thong);
		character.outfit[1].add(Clothing.miniskirt);
		character.closet.add(Clothing.Tshirt);
		character.closet.add(Clothing.thong);
		character.closet.add(Clothing.miniskirt);
		character.change(Modifier.normal);
		character.mod(Attribute.Seduction, 2);
		character.mod(Attribute.Perception, 1);
		character.getArousal().gainMax(10);
		character.money += 300;
		Global.gainSkills(character);
		character.add(Trait.female);
		character.add(Trait.undisciplined);
		character.add(Trait.experienced);
		character.add(Trait.lickable);
		character.setUnderwear(Trophy.AngelTrophy);
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.sneaking, 1);
		character.preferredSkills.add(FaceSit.class);
		character.preferredSkills.add(Fuck.class);
		character.preferredSkills.add(Taunt.class);
		character.preferredSkills.add(LustOverflow.class);
		character.preferredSkills.add(Flick.class);
		character.preferredSkills.add(Paizuri.class);
		character.preferredSkills.add(LustAura.class);
	}
	@Override
	public Skill act(HashSet<Skill> available,Combat c) {	
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
		for(Skill a:available){
			if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Ass Fuck")){
				mandatory.add(a);
			}
			if(character.is(Stsflag.orderedstrip)){
				if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
					mandatory.add(a);
				}
			}
		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&!c.hasModifier(Modifier.quiet)){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public void rest(int time, Daytime day) {
		if(character.rank>=1){
			if(character.money>0){
				day.visit("Black Market", character, Global.random(character.money));
			}
		}
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Onahole)||character.has(Toy.Onahole2))&&character.money>=300){
			character.gain(Toy.Onahole);
			character.money-=300;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2))&&character.money>=600&&character.getPure(Attribute.Seduction)>=20){
			character.gain(Toy.Strapon);
			character.money-=600;
		}
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		available.add("Play Video Games");
		if(character.rank>0){
			available.add("Workshop");
		}
		for(int i=0;i<time-4;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.AngelDWV, 1);
		}
		character.visit(2);
	}

	@Override
	public String bbLiner() {
		if(character.getAffection(Global.getPlayer())>=25){
			return "Angel smiles just a bit too sweetly. <i>\"Sorry lover, but by now you should know I'm a bit of a dom.\"</i>";
		}
		switch(Global.random(3)){
		case 1:
			return "Feigning remorse, Angel says <i>\"Sorry, cheap shots are all I can afford,\"</i> As she giggles sweetly at her own joke.";
		case 2:
			return "Angel cups her hands over her pussy, mocking your pain.  <i>\"Oh you silly little boys and your weak little balls.  That looks like it had to hurt.\"</i>";
		default:
			return "Angel seems to enjoy your anguish in a way that makes you more than a little nervous. <i>\"That's a great look for you, I'd like to see it more often.\"</i>";
		}
	}

	@Override
	public String nakedLiner() {
		return "Angel gives you a haughty look, practically showing off her body. <i>\"I can't blame you for wanting to see me naked, everyone does.\"</i>";
	}

	@Override
	public String stunLiner() {
		return "Angel groans on the floor. <i>\"You really are a beast. It takes a gentle touch to please a lady.\"</i>";
	}

	public String winningLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String taunt() {
		return "Angel pushes the head of your dick with her finger and watches it spring back into place. <i>\"You obviously can't help yourself. If only you were a little bigger, we could have a lot of fun.\"</i>";
	}

	@Override
	public void victory(Combat c, Tag flag) {
		character.arousal.empty();
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(opponent.human()) {
			if (flag == Result.anal) {
				if (Global.getValue(Flag.PlayerAssLosses) == 0) {
					Global.modCounter(Flag.PlayerAssLosses, 1);
					SceneManager.play(SceneFlag.AngelPeggingVictoryFirst);
				} else if (c.getOther(character).get(Attribute.Submissive) >= 10 && Global.getValue(Flag.PlayerAssLosses) >= 3 && Global.random(1) == 0) {
					Global.modCounter(Flag.PlayerAssLosses, 1);
					SceneManager.play(SceneFlag.AngelPeggingVictorySubmission);
				} else {
					Global.modCounter(Flag.PlayerAssLosses, 1);
					SceneManager.play(SceneFlag.AngelPeggingVictory);
				}
			} else if (c.stance.en == Stance.flying) {
				SceneManager.play(SceneFlag.AngelFlyingVictory);
			} else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.AngelSexVictory);
			} else if (character.pet != null) {
				SceneManager.play(SceneFlag.AngelImpVictory);
			} else if (character.has(Trait.succubus) && opponent.is(Stsflag.horny)) {
				SceneManager.play(SceneFlag.AngelHornyVictory);
			} else if (character.has(Trait.succubus) && Global.random(2) == 0) {
				c.getOther(character).add(new Drowsy(c.getOther(character), 4));
				character.add(new Energized(character, 10));
				SceneManager.play(SceneFlag.AngelSuccubusVictory);
			} else {
				if (Global.random(2) == 0) {
					SceneManager.play(SceneFlag.AngelForeplayVictoryAlt);
				} else {
					SceneManager.play(SceneFlag.AngelForeplayVictory);
				}

			}
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(opponent.human()) {
			if (flag == Result.anal) {
				SceneManager.play(SceneFlag.AngelAnalDefeat);
			} else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.AngelSexDefeat);
			} else if (c.lastact(opponent).hasTag(Result.kiss)) {
				SceneManager.play(SceneFlag.AngelKissDefeat);
			} else if (opponent.pet != null && opponent.pet.type() == Ptype.impmale) {
				SceneManager.play(SceneFlag.AngelImpDefeat);
			} else if (character.is(Stsflag.masochism)) {
				SceneManager.play(SceneFlag.AngelMasochismDefeat);
			} else if (character.has(Trait.succubus) && character.get(Attribute.Dark) >= 6 && Global.random(2) == 0) {
				SceneManager.play(SceneFlag.AngelSuccubusDefeat);
			} else if (Global.random(2) == 0) {
				SceneManager.play(SceneFlag.AngelForeplayDefeatAlt);
			} else {
				SceneManager.play(SceneFlag.AngelForeplayDefeat);
			}
		}
	}

	@Override
	public String describe() {
		if(character.getPure(Attribute.Spirituality)>0){
			return "Angel has added a shrine maiden's robe and wand over her demonic horns and wings. It's a lot more modest than how she is usually dressed. The contrast is strange, but beautiful, " +
					"like some some divine demon... or a fallen angel.";
		}
		else if(character.has(Trait.succubus)){
			return "Angel seems to have taken the path opposite her namesake. She has wings, but they're black as midnight. Small horns are visible through her hair and " +
					"a demonic tail sways lazily behind her. Her appearance should be frightening, but she's more beautiful and seductive than ever. Her entire being seems to " +
					"radiate sex and you struggle to ignore a treacherous little voice in the back of your mind that tells you to just give yourself to her.";
		}
		else{
			return "Angel has long, straight blonde hair that almost reaches her waist. She has a model's body: tall and very curvy, with impressively large breasts. " +
					"Beautiful, refined features complete the set, making her utterly irresistible. Her personality is prideful and overbearing, as though you belong to " +
					"her, but you don't know it yet.";
		}
	}

	@Override
	public void draw(Combat c, Tag flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(opponent.human()) {
			if (character.has(Trait.succubus) && Global.random(2) == 0) {
				SceneManager.play(SceneFlag.AngelSuccubusDraw);
			} else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.AngelSexDraw);
			} else {
				SceneManager.play(SceneFlag.AngelForeplayDraw);
			}
		}
	}
	@Override
	public boolean fightFlight(Character opponent) {
		return !character.nude()||opponent.nude();
	}
	@Override
	public boolean attack(Character opponent) {
		return true;
	}
	@Override
	public void ding() {
		if(character.getRank()>=4){
			character.mod(Attribute.Dark, 1);
			character.mod(Attribute.Spirituality, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(4);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
				else if(rand==3){
					character.mod(Attribute.Dark, 1);
				}
			}
		}else if(character.getPure(Attribute.Dark)>=1){
			character.mod(Attribute.Dark, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(4);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
				else if(rand==3){
					character.mod(Attribute.Dark, 1);
				}
			}
		}
		else{
			character.mod(Attribute.Seduction, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(3);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
			}
		}
		character.getStamina().gainMax(4);
		character.getArousal().gainMax(5);
		character.getMojo().gainMax(2);
		
	}
	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if(target.human()){
			return "Angel looks over your helpless body like a predator ready to feast. She kneels between your legs and teasingly licks your erection. She circles her " +
					"tongue around the head, coating your penis thoroughly with saliva. When she's satisfied that it is sufficiently lubricated and twitching with need, " +
					"she squeezes her ample breasts around your shaft. Even before she moves, the soft warmth surrounding you is almost enough to make you cum. When she " +
					"does start moving, it's like heaven. It takes all of your willpower to hold back your climax against the sensation of her wonderful bust rubbing against " +
					"your slick dick. When her tongue attacks your glans, poking out of her cleavage, it pushes you past the limit. You erupt like a fountain into her face, " +
					"while she tries to catch as much of your seed in her mouth as she can.";
		}
		else{
			if(target.hasDick()){
				return String.format("You present %s's naked, helpless form to Angel's tender ministrations. Angel licks her lips and delicately explores her victim's body "
						+ "with her fingers. She takes her time and makes several detours on the way, before arriving inevitably at %s's erect penis.<p>"
						+ "<i>\"Ooh, what a cute cock. I wonder how it tastes...\"</i> She should be talking to %s, but she's staring straight at you. She gives you "
						+ "a wink before breaking eye contact and for a brief moment, you imagine she'll push %s out of the way and suck you off instead. She doesn't, "
						+ "though, and soon %s is bucking %s hips at the mercy of Angel's skilled blowjob.<p>"
						+ "Angel sucks %s dry before giving you a seductive smile. <i>\"Are you feeling lonely? I don't mind giving you some service too.\"</i> You're "
						+ "sorely tempted to accept her offer. You can imagine how good her mouth would feel on your dick. However, you can't simply give her "
						+ "a free win. This is a competition after all.",
						target.name(),target.name(),target.name(),target.name(),target.name(),target.possessive(false),target.name(),target.pronounSubject(false) );
			}else{
				return "You present "+target.name()+"'s naked, helpless form to Angel's tender ministrations. Angel licks her lips and begins licking and stroking "+target.name()+"'s body. She's " +
					"hitting all the right spots, because soon "+target.name()+" is squirming and moaning in pleasure, and Angel hasn't even touched her pussy yet. " +
					"Angel meets your eyes to focus your attention and slowly moves her fingers down the front of "+target.name()+"'s body. You can't see her hands from " +
					"this position, but you know when she reaches her target, because "+target.name()+" immediately jumps as if she's been shocked. Soon it takes all of "+
					"your energy to control "+target.name()+" who is violently shaking in the throes of orgasm. You ease her to the floor as she goes completely limp, " +
					"while Angel licks the juice from her fingers.";
			}
		}
	}
	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "You manage to overwhelm "+assist.name()+" and bring her to the floor. You're able to grab both her arms and pin her helplessly beneath you. " +
					"Before you can take advantage of your position, pain explodes below your waist. "+assist.name()+" shouldn't have been able to reach your groin " +
					"from her position, but you're in too much pain to think about it. You are still lucid enough to feel large, perky breasts press against your back " +
					"and a soft whisper in your ear. <i>\"Surprise, lover.\"</i> The voice is unmistakably Angel's. She rolls you onto your back and positions herself over your face," +
					" with her legs pinning your arms. Her bare pussy is right in front of you, just out of reach of your tongue. It's weird that she's naked, considering " +
					"she caught you by surprise, but this is Angel after all.<p>";
		}
		else{
			return "You and "+target.name()+" grapple back and forth for several minutes. Soon you're both tired, sweaty, and aroused. You catch her hands for a moment and " +
					"run your tongue along her neck and collarbone. Recognizing her disadvantage, she jumps out of your grasp and directly into Angel. Neither of you " +
					"noticed Angel approach. Before "+target.name()+" can react, Angel pulls her into a passionate kiss. "+target.name()+" forgets to resist and goes limp " +
					"long enough for Angel to pin her arms.<p>";
		}
	}
	
	public void watched(Combat c, Character target, Character viewer){
		if (viewer.human()) {
			if (character.has(Trait.succubus)) {
				SceneManager.play(SceneFlag.AngelWatchDark, target);
			} else {
				SceneManager.play(SceneFlag.AngelWatch, target);
			}
		}
	}
	
	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case icequeen:
				return "Angel gives you a look that chills you to the bone. <i>\"Don't get full of yourself just because you gave me a good time. "
						+ "Believe me, I have plenty of experience resisting men's advances. Your sweet talk won't get you anywhere.\"</i>";
			case seductress:
				return "Angel licks her lips and runs a finger down her body. Her sultry smile is somehow even sexier than usual. <i>\"I've been "
						+ "slipping a bit since I'm getting so much sex lately, but if I put the effort in, I can always get who I want.\"</i>";
			case untouchable:
				return "Angel begins stretching as you prepare for the fight. Wow, she's more flexible than you realized. <i>\"You better believe it. "
						+ "I can be very hard to get when I want to be. Last time you won because I let your fingers, tongue, and other bits touch me. "
						+ "That's not going to happen this time. I'm going to be the one touching you!\"</i>";
			case succubusvagina:
				return "Angel grins at you as her hand strays down to her pelvis. <i>\"You actually made me cum first during sex. Want to try it again?\"</i> "
						+ "Her pointed tail sways playfully, drawing your attention to her inhuman features.<br>"
						+ "<i>\"Do you want to see what these powers can do to my pussy when I'm prepared? I can't wait to show you.\"</i>";
			default:
				break;
			}
		}if(character.nude()){
			return "Angel poses sexily, flaunting her naked breasts. <i>\"Do you like what you see? If you sit still and behave yourself, I'll "
					+ "let you feel these soft tits around your dick.\"</i>";
		}
		if(opponent.pantsless()){
			return "Angel eyes your exposed dick hungrily. <i>\"Did I catch you at a bad time, lover? Or maybe you just want me to drain you "
					+ "dry. I don't mind one bit.\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Angel's smile softens for a moment as she looks at you, but she quickly regains her edge. <i>\"I was hoping to see you, lover. "
					+ "You're just so much fun to fuck.\"</i>";
		}
		if(character.has(Trait.succubus)){
			return "Angel spreads her black wings and gives you a predatory grin. <i>\"Are you ready to cum for me?\"</i>";
		}
		
		return "Angel licks her lips and stalks you like a predator.";
	}
	@Override
	public boolean fit() {
		return !character.nude()&&character.getStamina().percent()>=50;
	}
	@Override
	public boolean night() {
		Global.gui().loadPortrait(Global.getPlayer(), this.character);
		SceneManager.play(SceneFlag.AngelAfterMatch);
		return true;
	}
	public void advance(int rank){
		if (rank >= 4 && character.getPure(Attribute.Spirituality)==0){
			character.mod(Attribute.Spirituality,1);
			character.outfit[Character.OUTFITTOP].removeAllElements();
			character.outfit[Character.OUTFITBOTTOM].removeAllElements();
			character.outfit[Character.OUTFITTOP].add(Clothing.wrap);
			character.outfit[Character.OUTFITTOP].add(Clothing.shrinerobe);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.ofuda);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.shrineskirt);
			character.closet.add(Clothing.wrap);
			character.closet.add(Clothing.shrinerobe);
			character.closet.add(Clothing.ofuda);
			character.closet.add(Clothing.shrineskirt);
			character.clearSpriteImages();
		}
		if(rank >= 3 && !character.has(Trait.lustconduit)){
			character.add(Trait.lustconduit);
		}
		if(rank >= 2 && !character.has(Trait.scandalous)){
			character.add(Trait.scandalous);
		}
		if(rank >= 1 && !character.has(Trait.succubus)){
			character.add(Trait.succubus);
			character.add(Trait.tailed);
			character.outfit[0].removeAllElements();
			character.outfit[1].removeAllElements();
			character.outfit[0].add(Clothing.bikinitop);
			character.outfit[1].add(Clothing.bikinibottoms);
			character.closet.add(Clothing.bikinitop);
			character.closet.add(Clothing.bikinibottoms);
			character.mod(Attribute.Dark,1);
			character.clearSpriteImages();
		}
		
	}
	@Override
	public NPC getCharacter() {
		return character;
	}
	
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case horny:
			return value>=50;
		case nervous:
			return value>=150;
		default:
			return value>=100;
		}
	}
	@Override
	public String image() {
		return "assets/angel_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case horny:
			return 1.2f;
		case nervous:
			return .7f;
		default:
			return 1f;
		}
	}
	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Mmmm, are you going to fill me up now?\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"No! I can't lose like this! Cum now! Oh!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Keep going! Fill me with your cum!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Oh fuck! Why aren't you cumming yet?\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"I knew you were a sucker for ass. Fill me!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Ah! That's a new feeling... but it's a good feeling!\"</i>");
		comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Shall I make you cum now? Are you going to cum all over yourself?\"</i>");
		comments.put(CommentSituation.SIXTYNINE_WIN, "<i>\"Think you can outlast me? Better get licking.\"</i>");
		comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Hello lover. You're all mine now...\"</i>");
		comments.put(CommentSituation.BEHIND_SUB_WIN, "<i>\"What are you doing back there, naughty boy?\"</i>");
		comments.put(CommentSituation.OTHER_CHARMED, "<i>\"You want to please me, don't you? Your beautiful Angel?\"</i>");
		comments.put(CommentSituation.OTHER_ENTHRALLED, "<i>\"What a fun little pet you are...\"</i>");
		comments.put(CommentSituation.OTHER_BOUND, "<i>\"You're all wrapped up like a present... It's not even my birthday.\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"I need you! Now! Put your cock in me!\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"You're a little hot for me aren't you? I can help with that.\"</i>");
		comments.put(CommentSituation.SELF_OILED, "<i>\"I'm all wet... Slimy... Slippery... Shiny... Do you like me like this?\"</i>");
		comments.put(CommentSituation.OTHER_SHAMED, "<i>\"Are you embarrassed? But your cock looks so eager...\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Angel seems deeply indignant; she narrows her eyes and gives you an icy glare, trying to hide her grimace.");
		comments.put(new SkillComment(Result.oral,true),"<i>\"Ahh! I love your tongue!\"</i>");
		comments.put(new SkillComment(Attribute.Dark,true),"<i>\"Dark powers, huh? Don't you love that tingle you get when you unleash them?\"</i>");
		comments.put(new SkillComment(Attribute.Fetish,true),"<i>\"You're using Fetish magic? Even I'm not kinky enough for that.\"</i>");
		comments.put(new SkillComment(Attribute.Spirituality,true),"<i>\"Getting better at your purification techniques? It'll be good practice to try to break through them.\"</i>");
		comments.put(new SkillComment(SkillTag.PET,false),"<i>\"Go, my thrall. Help me bring this boy to his knees.\"</i>");
		return comments;
	}

	@Override
	public CommentGroup getResponses() {
		CommentGroup comments = new CommentGroup();
		return comments;

	}

	@Override
	public int getCostumeSet() {
		if(character.has(Trait.succubus)){
			return 2;
		}else{
			return 1;
		}
	}
	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if(c.eval(character)== Result.intercourse && character.has(Trait.succubus)) {
			character.addGrudge(opponent,Trait.succubusvagina);
		}else if(character.getGrudge()==Trait.icequeen || character.getGrudge()==Trait.seductress){
			character.addGrudge(opponent,Trait.untouchable);
		}else{		
			switch(Global.random(2)){
			case 0:
				character.addGrudge(opponent,Trait.icequeen);
				break;
			case 1:
				character.addGrudge(opponent,Trait.seductress);
				break;
			default:
				break;
			}
		}
		
	}

	@Override
	public void resetOutfit() {
		character.outfit[0].clear();
		character.outfit[1].clear();
		if(character.getPure(Attribute.Spirituality)>0){
			character.outfit[Character.OUTFITTOP].add(Clothing.wrap);
			character.outfit[Character.OUTFITTOP].add(Clothing.shrinerobe);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.ofuda);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.shrineskirt);
		}else if(character.has(Trait.succubus)){
			character.outfit[0].add(Clothing.bikinitop);
			character.outfit[1].add(Clothing.bikinibottoms);
		}else{
			character.outfit[0].add(Clothing.Tshirt);
			character.outfit[1].add(Clothing.thong);
			character.outfit[1].add(Clothing.miniskirt);
		}

	}
}
