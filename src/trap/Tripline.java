package trap;

import global.Scheduler;
import items.Component;
import items.Item;
import stance.StandingOver;
import global.Global;
import combat.Combat;
import combat.Encounter;

import characters.Attribute;
import characters.Character;
import characters.Anatomy;

public class Tripline extends Trap {
	public Tripline(){
		super("Tripline","Will trip the victim, letting you start the fight in an advantageous position",
				"You run a length of rope at ankle height. It should trip anyone who isn't paying much attention.", 3);
		recipe.put(Component.Rope,1);
	}
	@Override
	public void trigger(Character target) {
		if(target.human()){
			if(!target.check(Attribute.Perception, 20-(target.get(Attribute.Perception)+target.bonusDisarm()))){
				Global.gui().message("You trip over a line of cord and fall on your face.");
				target.pain(5,Anatomy.head);
				target.location().opportunity(target,this);
			}
			else{
				Global.gui().message("You spot a line strung across the corridor and carefully step over it.");
				target.location().remove(this);
			}
		}
		else{
			if(!target.check(Attribute.Perception, 15)){
				if(target.location().humanPresent()){
					Global.gui().message(target.name()+" carelessly stumbles over the tripwire and lands with an audible thud.");
				}
				target.pain(5,Anatomy.head);
				target.location().opportunity(target,this);
			}
		}
	}

	@Override
	public boolean requirements(Character owner) {
		return true;
	}

	@Override
	public Trap copy() {
		return new Tripline();
	}

}
