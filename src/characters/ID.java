package characters;

public enum ID {
    PLAYER,
    CASSIE,
    MARA,
    ANGEL,
    JEWEL,
    YUI,
    KAT,
    REYKA,
    EVE,
    SAMANTHA,
    MAYA,
    VALERIE,
    SELENE,
    JUSTINE,
    AESOP,
    LILLY,
    AISHA,
    SUZUME,
    JETT,
    RIN,
    ALICE,
    GINETTE,
    CAROLINE,
    SARAH,
    MEI,
    SOFIA,
    CUSTOM1,
    CUSTOM2,
    CUSTOM3,
    CUSTOM4,
    CUSTOM5;


    public static ID fromString(String name){
        if(name.equalsIgnoreCase( "Cassie")){
            return CASSIE;
        }else if(name.equalsIgnoreCase("Mara")){
            return MARA;
        }else if(name.equalsIgnoreCase("Angel")){
            return ANGEL;
        }else if(name.equalsIgnoreCase("Jewel")){
            return JEWEL;
        }else if(name.equalsIgnoreCase("Yui")){
            return YUI;
        }else if(name.equalsIgnoreCase("Kat")){
            return KAT;
        }else if(name.equalsIgnoreCase("Reyka")){
            return REYKA;
        }else if(name.equalsIgnoreCase("Eve")){
            return EVE;
        }else if(name.equalsIgnoreCase("Samantha")){
            return SAMANTHA;
        }else if(name.equalsIgnoreCase("Maya")){
            return MAYA;
        }else if(name.equalsIgnoreCase("Valerie")){
            return VALERIE;
        }else if(name.equalsIgnoreCase("Sofia")) {
            return SOFIA;
        }else if(name.equalsIgnoreCase("Selene")) {
            return SELENE;
        }else {
            return PLAYER;
        }
    }
}
