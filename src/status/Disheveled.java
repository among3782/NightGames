package status;

import characters.Character;
import characters.Trait;
import combat.Combat;
import items.Clothing;

public class Disheveled extends Status{
    private Clothing article;

    public Disheveled(Character afflicted, Clothing article){
        super("Disheveled: "+article.getProperName(),afflicted);
        tooltip = "Increased chance to strip";
        this.duration=4;
        if(affected!=null && affected.has(Trait.PersonalInertia)){
            this.duration=6;
        }
        this.article = article;
    }

    @Override
    public String describe() {
        if(affected.human()){
            return "Your "+article.getName()+" almost fell off with that last attempt.";
        }
        else{
            return affected.name()+" is struggling to keep her "+article.getName()+" on.";
        }
    }

    @Override
    public int stripAttempt(Clothing article) {
        if(this.article == article){
            return 20;
        }
        return 0;
    }

    @Override
    public Status copy(Character target) {
        return new Disheveled(target,article);
    }

    @Override
    public void turn(Combat c) {
        if(affected.isWearing(article.getType())){
            decay();
        }else{
            affected.removeStatus(this,c);
        }
    }
}
