package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Stsflag;

public class TentacleCaress extends Skill {
    public TentacleCaress(Character self) {
        super("Tentacle Caress", self);
        addTag(Attribute.Eldritch);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Eldritch)>=3;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return !self.stunned() && !self.distracted() && !self.is(Stsflag.enthralled) && target.pantsless();
    }

    @Override
    public String describe() {
        return null;
    }

    @Override
    public void resolve(Combat c, Character target) {

    }

    @Override
    public Skill copy(Character user) {
        return new TentacleCaress(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return null;
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return null;
    }
}
