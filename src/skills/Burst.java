package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Pool;
import combat.Combat;
import combat.Result;
import stance.Neutral;
import status.Stsflag;

import static stance.Stance.neutral;

public class Burst extends Skill{

    public Burst(Character self){
        super("Burst",self);
        addTag(Attribute.Ki);
        addTag(SkillTag.ESCAPE);
    }
    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Ki)>=24;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return ((!c.stance.mobile(self)&&c.stance.sub(self))||self.bound())&&!self.stunned()&&!self.distracted()&&!self.is(Stsflag.enthralled) && self.canSpend(Pool.STAMINA,30);
    }

    @Override
    public String describe() {
        return "Release a burst of Ki to break out of a hold: 30 Stamina";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.STAMINA,30);
        if(self.human()){
            c.write(self,deal(0, Result.normal,target));
        }
        else if(target.human()){
            c.write(self,receive(0, Result.normal,target));
        }

        c.stance=new Neutral(self,target);
        target.emote(Emotion.nervous, 10);
        self.free();
    }

    @Override
    public Skill copy(Character user) {
        return new Burst(user);
    }

    @Override
    public Tactics type() {
        return Tactics.positioning;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "You release an intense burst of Ki, exhausting yourself, but knocking "+target.name()+" away.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return self.name()+" grunts with exertion, and you are suddenly knocked away by a burst of energy.";
    }
}
