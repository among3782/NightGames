package Comments;

import characters.Anatomy;
import characters.Character;
import combat.Combat;
import combat.CombatEvent;
import combat.Result;

public class LowBlowRequirement implements CustomRequirement {

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		for(CombatEvent event : c.getEvents(self)){
			if(event.getEvent()== Result.receivepain && event.getPart()== Anatomy.genitals){
				return true;
			}
		}
		return false;
	}

}
