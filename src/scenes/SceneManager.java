package scenes;

import characters.ID;
import characters.Character;
import global.Global;
import global.Roster;

import java.util.HashMap;

public class SceneManager {
    public static SceneManager instance;
    private static HashMap<ID,SceneTable> scenedb;

    public SceneManager(){
        scenedb = new HashMap<ID,SceneTable>();
        scenedb.put(ID.CASSIE,new CassieScenes());
        scenedb.put(ID.MARA,new MaraScenes());
        scenedb.put(ID.ANGEL,new AngelScenes());
        scenedb.put(ID.JEWEL,new JewelScenes());
        scenedb.put(ID.YUI,new YuiScenes());
        scenedb.put(ID.KAT,new KatScenes());
        scenedb.put(ID.REYKA,new ReykaScenes());
        scenedb.put(ID.EVE,new EveScenes());
        scenedb.put(ID.VALERIE,new ValerieScenes());
        scenedb.put(ID.SAMANTHA,new SamanthaScenes());
        scenedb.put(ID.SOFIA,new SofiaScenes());
        scenedb.put(ID.SELENE,new SeleneScenes());
        scenedb.put(ID.PLAYER,new MiscScenes());
        scenedb.put(ID.MAYA,new MayaScenes());
        scenedb.put(ID.AESOP,new AesopScenes());


    }

    private static SceneManager getInstance(){
        if(instance==null){
            instance = new SceneManager();
        }
        return instance;
    }

    public static void play(SceneFlag scene){
        play(scene,null);
    }

    public static void play(SceneFlag scene, Character npc){
        SceneManager.getInstance().playScene(scene,npc);
    }
    public void playScene(SceneFlag scene,Character npc){
        if(scenedb.containsKey(scene.getStar())){
            Scene playing = scenedb.get(scene.getStar()).getScene(scene);
            playing.parse(Roster.get(ID.PLAYER),npc);
            playing.play(0);
        }else{
            Global.gui().message("Error: scene table not found for ID: "+scene.getStar());
        }
        Global.watched(scene);
    }

    public static int getTotalCount(ID star, SceneType type){
        int count = 0;
        for(SceneFlag scene: SceneFlag.values()){
            if(scene.getStar()==star && scene.getType()==type){
                count++;
            }
        }
        return count;
    }
}
