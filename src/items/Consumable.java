package items;

import java.util.ArrayList;

import characters.Character;

public enum Consumable implements Item {
	ZipTie		( "Heavy Zip Tie",5,"A thick heavy tie suitable for binding someone's hands","a "),
	Handcuffs	( "Handcuffs",200,"Strong steel restraints, hard to escape from",""),
	Talisman	( "Dark Talisman",100,"This unholy trinket can cloud a target's mind","a "),
	FaeScroll	( "Summoning Scroll",150,"A magic scroll imbued with summoning magic. It can briefly support a large number of faeries","a "),
	needle		( "Drugged Needle",10,"A small throwing needle coated in a potent aphrodisiac","a "),
	smoke		( "Smoke Bomb",20,"This harmless bomb provides cover for a quick escape","a "),
	powerband	( "Power Band",250, "This headband is a catalyst of great power, temporarily unleashing the wearer's maximum Ki", "a "),
	
	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	private int price;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public String getFullDesc(Character owner) {
		return getDesc();
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String getFullName(Character owner){
		return getName();
	}
	public String pre(){
		return prefix;
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	@Override
	public Boolean listed() {
		return true;
	}
	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<Item>();
	}
	private Consumable( String name, int price, String desc,String prefix )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
	}
}
