package characters;

import java.io.Serializable;
import java.util.HashSet;

import Comments.CommentGroup;
import daytime.Daytime;
import global.Match;
import skills.Skill;

import combat.Combat;
import combat.Tag;

import actions.Action;
import actions.Movement;


public interface Personality extends Serializable{
	public Skill act(HashSet<Skill> available,Combat c);
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match);
	public NPC getCharacter();
	public void rest(int time, Daytime day);
	public String bbLiner();
	public String nakedLiner();
	public String stunLiner();
	public String taunt();
	public void victory(Combat c, Tag flag);
	public void defeat(Combat c, Tag flag);
	public String victory3p(Combat c,Character target, Character assist);
	public String intervene3p(Combat c,Character target, Character assist);
	public String resist3p(Combat c,Character target, Character assist);
	public void watched(Combat c, Character target, Character viewer);
	public String describe();
	public void draw(Combat c, Tag flag);
	public boolean fightFlight(Character opponent);
	public boolean attack(Character opponent);
	public void ding();
	public String startBattle(Character opponent);
	public boolean fit();
	public boolean night();
	public void advance(int Rank);
	public boolean checkMood(Emotion mood, int value);
	public float moodWeight(Emotion mood);
	public String image();
	public void pickFeat();
	public CommentGroup getComments();
	public CommentGroup getResponses();
	public int getCostumeSet();
	public void declareGrudge(Character opponent, Combat c);
	public void resetOutfit();
}
