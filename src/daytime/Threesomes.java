package daytime;

import java.time.LocalTime;
import java.util.HashMap;

import characters.*;
import characters.Character;
import global.Flag;
import global.Global;
import global.Roster;
import global.Scheduler;
import scenes.Event;
import scenes.SceneFlag;
import scenes.SceneManager;

public class Threesomes implements Event {
	private Character player;
	private Dummy angel;
	private Dummy mara;
	private Dummy cassie;
	private Dummy jewel;
	private Dummy reyka;
	private Dummy kat;
	private Dummy yui;
	private Boolean proc;
	public Threesomes(Character player) {
		this.player = player;
		angel = new Dummy("Angel");
		mara = new Dummy("Mara");
		cassie = new Dummy("Cassie");
		jewel = new Dummy("Jewel");
		reyka = new Dummy("Reyka");
		kat = new Dummy("Kat");
		yui = new Dummy("Yui");
		proc = false;
	}

	
	public boolean play(String choice) {
		
		if(choice=="CassieJewel"){
			cassie.undress();
			cassie.setBlush(3);
			jewel.undress();
			jewel.setBlush(3);
			jewel.setMood(Emotion.dominant);
			Global.gui().loadTwoPortraits(jewel, cassie);
			SceneManager.play(SceneFlag.CassieJewelThreesome);
		}
		if(choice=="CassieAngel"){
			cassie.undress();
			cassie.setBlush(3);
			cassie.setMood(Emotion.nervous);
			angel.undress();
			angel.setBlush(3);
			Global.gui().loadTwoPortraits(cassie, angel);
			SceneManager.play(SceneFlag.CassieAngelThreesome);
		}
		if(choice=="MaraJewel"){
			jewel.setBlush(2);
			jewel.setMood(Emotion.dominant);
			mara.setBlush(2);
			mara.setMood(Emotion.dominant);
			Global.gui().loadTwoPortraits(jewel, mara);
			SceneManager.play(SceneFlag.MaraJewelThreesome);
		}
		if(choice=="CassieMara"){
			cassie.undress();
			cassie.setBlush(3);
			cassie.setMood(Emotion.horny);
			mara.undress();
			mara.setBlush(3);
			Global.gui().loadTwoPortraits(cassie, mara);
			SceneManager.play(SceneFlag.CassieMaraThreesome);
		}
		if(choice=="AngelMara"){
			mara.undress();
			mara.setBlush(3);
			mara.setMood(Emotion.horny);
			angel.undress();
			angel.setBlush(3);
			angel.setMood(Emotion.horny);
			Global.gui().loadTwoPortraits(mara, angel);
			SceneManager.play(SceneFlag.AngelMaraThreesome);
		}
		if(choice == "AngelReyka"){
			angel.undress();
			angel.setBlush(3);
			angel.setMood(Emotion.horny);
			reyka.undress();
			reyka.setBlush(3);
			Global.gui().loadTwoPortraits(angel, reyka);
			SceneManager.play(SceneFlag.AngelReykaThreesome);
		}
		if(choice == "CassieKat"){
			cassie.undress();
			cassie.setBlush(3);
			kat.undress();
			kat.setBlush(3);
			Global.gui().loadTwoPortraits(cassie, kat);
			SceneManager.play(SceneFlag.CassieKatThreesome);
		}
		if(choice == "AngelReykaImp"){
			angel.undress();
			angel.setBlush(3);
			angel.setMood(Emotion.horny);
			reyka.undress();
			reyka.setBlush(3);
			Global.gui().loadTwoPortraits(angel, reyka);
			if(Roster.get(ID.ANGEL).get(Attribute.Dark)+Global.random(10)>Global.getPlayer().get(Attribute.Dark)+Global.random(10)){
				SceneManager.play(SceneFlag.AngelReykaThreesome2a);
			}else{
				SceneManager.play(SceneFlag.AngelReykaThreesome2);
			}
		}
		if(choice == "MaraKat") {
			mara.undress();
			mara.setBlush(3);
			kat.undress();
			kat.setBlush(3);
			Global.gui().loadTwoPortraits(mara, kat);
			SceneManager.play(SceneFlag.MaraKatThreesome);
		}
		if(choice=="CassieYui"){
			cassie.undress();
			cassie.setMood(Emotion.horny);
			yui.dress();
			yui.setMood(Emotion.horny);
			SceneManager.play(SceneFlag.CassieYuiThreesome);
		}if(choice.equalsIgnoreCase("AngelJewel")){
			jewel.setTopOuter(false);
			jewel.setTopInner(false);
			jewel.setMood(Emotion.angry);
			jewel.setBlush(2);
			angel.setMood(Emotion.dominant);
			angel.setBlush(2);
			SceneManager.play(SceneFlag.AngelJewelThreesome);
		}
		Global.flag(Flag.threesome);
		Global.modCounter(Flag.ThreesomeCnt, 1);
		
		Global.current=this;
		Global.gui().choose("Next");
		return true;
	}

	@Override
	public void respond(String response) {
		Global.gui().clearText();
		Scheduler.advanceTime(LocalTime.of(1,0));
		Scheduler.getDay().plan();
	}

	@Override
	public String morning() {
		return "";
	}


	@Override
	public String mandatory() {
		return "";
	}


	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		if(Global.checkFlag(Flag.threesome)){
			return;
		}
		if(Roster.getAffection(ID.PLAYER,ID.CASSIE)>=20&&Roster.getAffection(ID.PLAYER,ID.JEWEL)>=20&&Roster.getAffection(ID.JEWEL,ID.CASSIE)>=5&&
				Global.getValue(Flag.CassieDWV)>=5){
			available.put("CassieJewel",4);
		}
		if(Roster.getAffection(ID.PLAYER,ID.MARA)>=20&&Roster.getAffection(ID.PLAYER,ID.JEWEL)>=20&&Roster.getAffection(ID.MARA,ID.JEWEL)>=5){
			available.put("MaraJewel",4);
		}
		if(Roster.getAffection(ID.PLAYER,ID.MARA)>=15&&Roster.getAffection(ID.PLAYER,ID.ANGEL)>=15&&Roster.getAffection(ID.MARA,ID.ANGEL)>=10){
			available.put("AngelMara",4);
		}
		if(Roster.getAffection(ID.PLAYER,ID.MARA)>=15&&Roster.getAffection(ID.PLAYER,ID.CASSIE)>=15&&Roster.getAffection(ID.MARA,ID.CASSIE)>=10){
			available.put("CassieMara",4);
		}
		if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>=20&&Roster.getAffection(ID.PLAYER,ID.CASSIE)>=20&&Roster.getAffection(ID.ANGEL,ID.CASSIE)>=10){
			available.put("CassieAngel",4);
		}
		if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>=20&&Roster.getAffection(ID.PLAYER,ID.REYKA)>=20&&Roster.getAffection(ID.REYKA,ID.ANGEL)>=10){
			available.put("AngelReyka",4);
		}
		if(Roster.getAffection(ID.PLAYER,ID.KAT)>=20&&Roster.getAffection(ID.PLAYER,ID.CASSIE)>=20&&Roster.getAffection(ID.KAT,ID.CASSIE)>=10){
			available.put("CassieKat",4);
		}
        if(Roster.getAffection(ID.PLAYER,ID.KAT)>=20&&Roster.getAffection(ID.PLAYER,ID.MARA)>=20&&Roster.getAffection(ID.KAT,ID.MARA)>=10){
            available.put("MaraKat",4);
        }
		if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>=20&&Roster.getAffection(ID.PLAYER,ID.REYKA)>=20&&Roster.getAffection(ID.REYKA,ID.ANGEL)>=10&&player.getPure(Attribute.Dark)>=10){
			available.put("AngelReykaImp",2);
		}
		if(Roster.getAffection(ID.PLAYER,ID.YUI)>=20&&Roster.getAffection(ID.PLAYER,ID.YUI)>=20&&Roster.getAffection(ID.CASSIE,ID.YUI)>=10){
		    available.put("CassieYui",4);
        }
		if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>=20&&Roster.getAffection(ID.PLAYER,ID.JEWEL)>=20&&Roster.getAffection(ID.ANGEL,ID.JEWEL)>=10){
			available.put("AngelJewel",4);
		}
	}

}
