package Comments;

import characters.Character;
import combat.Combat;

public interface CommentTrigger {

    public boolean isApplicable(Combat c, Character self, Character other);
    public int getPriority();
    public int getProbability();
    public boolean equals(Object o);

}
