package scenes;

import java.util.HashMap;

public interface Event {
	public void respond(String response);
	public boolean play(String response);
	public String morning();
	public String mandatory();
	public void addAvailable(HashMap<String,Integer> available);
}
