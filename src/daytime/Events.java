package daytime;

import java.util.ArrayList;
import java.util.HashMap;

import global.Global;
import characters.Character;
import scenes.AngelEvent;
import scenes.MaraEvent;
import scenes.Event;

public class Events {
	private ArrayList<HashMap<String,Integer>> available;
	private ArrayList<Event> scenes;
	public Events(Character player, Daytime day) {
		scenes = new ArrayList<Event>();
		available = new ArrayList<HashMap<String,Integer>>();
		scenes.add(new Threesomes(player));
		scenes.add(new MaraEvent(player));
		scenes.add(new AngelEvent(player));
		scenes.add(new MorningEvents(player,day));

		for(int i = 0; i<scenes.size();i++){
			available.add(new HashMap<String,Integer>());
			scenes.get(i).addAvailable(available.get(i));
		}
	}

	public boolean checkMorning(){
		String mand;
		for(Event events: scenes){
			mand = events.morning();
			if(mand!=""){
				return events.play(mand);
			}
		}
		return false;
	}
	public boolean checkScenes(){
		String mand;
		for(int i = 0; i<scenes.size();i++){
			mand = scenes.get(i).mandatory();
			if(mand != ""){
				scenes.get(1).play(mand);
				return true;
			}
		}
		available.clear();
		for(int i = 0; i<scenes.size();i++){
			available.add(new HashMap<String,Integer>());
			scenes.get(i).addAvailable(available.get(i));
		}
		for(int i = 0; i<scenes.size();i++){
			for(String scene: available.get(i).keySet()){
				if(Global.random(100)<available.get(i).get(scene)){
					return scenes.get(i).play(scene);
				}	
			}
		}
		return false;
	}

}
