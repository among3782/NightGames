package combat;

import characters.Anatomy;
import skills.Skill;

public class CombatEvent {
    private Result event;
    private Skill skill;
    private Anatomy part;

    public CombatEvent(Result event){
        this.event = event;
    }
    public CombatEvent(Result event, Skill skill){
        this(event);
        this.skill = skill;
    }

    public CombatEvent(Result event, Anatomy part){
        this(event);
        this.part = part;
    }

    public Result getEvent(){ return event; }

    public Skill getSkill(){ return skill; }

    public Anatomy getPart(){ return part; }
}
