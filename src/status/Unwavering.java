package status;

import characters.Character;
import characters.Trait;

public class Unwavering extends Status{
    public Unwavering(Character affected){
        super("Unwavering",affected);
        flag(Stsflag.unwavering);
        this.traits.add(Trait.temptimmune);
        tooltip = "Immune to temptation";
        this.duration=4;
        if(affected!=null && affected.has(Trait.PersonalInertia)){
            this.duration=6;
        }
    }

    @Override
    public String describe() {
        if(affected.human()){
            return "Your unwavering heart ignores all temptation.";
        }
        else{
            return affected.name()+" is completely serene.";
        }
    }

    @Override
    public Status copy(Character target) {
        return new Unwavering(target);
    }
}
