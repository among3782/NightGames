package actions;

import global.Scheduler;
import trap.Trap;
import global.Global;
import global.Modifier;
import characters.Character;

public class SetTrap extends Action {
	private Trap trap;
	public SetTrap(Trap trap) {
		super("Set("+trap.toString()+")");
		this.trap=trap;
	}

	@Override
	public boolean usable(Character user) {
		return trap.recipe(user)&&!user.location().open()&&trap.requirements(user)
				&&user.location().env.size()<5
				&&(!user.human()|| Scheduler.getMatch().condition!=Modifier.noitems);
	}

	@Override
	public Movement execute(Character user) {
		Trap copy = trap.place(user, user.location());
		String message = copy.setup();
		if(user.human()){
			Global.gui().message(message);
		}

		return Movement.trap;
	}

	@Override
	public Movement consider() {
		return Movement.trap;
	}

}
