package skills;

import combat.Result;
import stance.*;
import characters.Attribute;
import characters.Character;
import characters.Emotion;

import combat.Combat;

public class Restrain extends Skill {

	public Restrain(Character self) {
		super("Pin", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&c.stance.prone(target)&&self.canAct()&&!c.stance.penetration(self)&&!c.stance.penetration(target)&&
				(c.stance.enumerate()==Stance.mount||c.stance.enumerate()==Stance.reversemount);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(c.effectRoll(this, self, target,self.get(Attribute.Power)/3)){
			if(self.getPure(Attribute.Ki)>=12){
				if (c.stance.enumerate() == Stance.reversemount) {
					c.stance = new SubmissionPin(self, target);
					if (self.human()) {
						c.write(self, deal(1, Result.special, target));
					} else if (target.human()) {
						c.write(self, receive(1, Result.special, target));
					}
				} else {
					c.stance = new PleasurePin(self, target);
					if (self.human()) {
						c.write(self, deal(0, Result.special, target));
					} else if (target.human()) {
						c.write(self, receive(0, Result.special, target));
					}
				}
			}else {
				if (self.human()) {
					c.write(self, deal(0, Result.normal, target));
				} else if (target.human()) {
					c.write(self, receive(0, Result.normal, target));
				}
				if (c.stance.enumerate() == Stance.reversemount) {
					c.stance = new ReversePin(self, target);
				} else {
					c.stance = new Pin(self, target);
				}
			}
			target.emote(Emotion.nervous, 10);
			target.emote(Emotion.desperate, 10);
			self.emote(Emotion.dominant, 20);
		}
		else{
			if(self.human()){
				c.write(self,deal(0, Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0, Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=8;
	}

	@Override
	public Skill copy(Character user) {
		return new Restrain(user);
	}
	public int speed(){
		return 2;
	}
	public int accuracy(){
		return 4;
	}
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.special) {
			if(damage == 1){
				return "You restrain "+target.name()+"'s arms and put intense pressure on her groin.";
			}else{
				return "You grab "+target.name()+"'s wrists and press your thigh between her legs.";
			}
		}else if(modifier == Result.miss){
			return "You try to catch "+target.name()+"'s hands, but she squirms to much to keep your grip on her.";
		}
		else{
			return "You manage to restrain "+target.name()+", leaving her helpless and vulnerable beneath you.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.special) {
			if(damage == 1){
				return self.name()+" pins your arms with her thighs and grabs you by the balls.";
			}else{
				return self.name()+" pins your hands and presses her knee into your groin, leaving you at her mercy.";
			}
		}else if(modifier == Result.miss){
			return self.name()+" tries to pin you down, but you keep your arms free.";
		}
		else{
			return self.name()+" pounces on you and pins your arms in place, leaving you at her mercy.";
		}
	}

	@Override
	public String describe() {
		return "Restrain opponent until she struggles free";
	}
}
