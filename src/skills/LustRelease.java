package skills;

import characters.Attribute;
import characters.Character;
import characters.ID;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Horny;

public class LustRelease extends Skill{

    public LustRelease(Character self){
        super("Lust Release",self);
        addTag(Attribute.Unknowable);

    }
    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Unknowable)>=21;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c) && self.canSpend(Pool.ENIGMA,2) && self.getArousal().percent()>=50;
    }

    @Override
    public String describe() {
        return "Transfer your arousal to your opponent: 2 Enigma";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.ENIGMA,2);
        if(target.get(Attribute.Dark)>=10){
            if(self.human()){
                c.write(self,deal(0, Result.miss,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.miss,target));
            }
        }else if(c.isWatching(ID.REYKA)){
            if(self.human()){
                c.write(self,deal(0, Result.defended,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.defended,target));
            }
        }else if(c.isWatching(ID.ANGEL)){
            if(self.human()){
                c.write(self,deal(1, Result.defended,target));
            }
            else if(target.human()){
                c.write(self,receive(1, Result.defended,target));
            }
        }else{
            int lust = Math.min(self.getArousal().get(),self.get(Attribute.Unknowable)*3);
            self.calm(lust);
            target.add(new Horny(target,lust/4,4));
            if(self.human()){
                c.write(self,deal(0, Result.normal,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.normal,target));
            }
        }
    }

    @Override
    public Skill copy(Character user) {
        return new LustRelease(user);
    }

    @Override
    public Tactics type() {
        return Tactics.calming;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "";
        }else if(modifier == Result.defended){
            if(damage ==1){
                return "";
            }else{
                return "";
            }
        }else{
            return "";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return self.name()+" takes a deep breath and you feel a haze of energy radiate out from her. An aura of pure arousal " +
                    "leaks out, so dense you can see it. Fortunately, you have enough Dark knowledge to manipulate such tangible lust. " +
                    "You force the aura back into "+self.name()+" making "+self.pronounSubject(false)+" shiver " +
                    "with need.";
        }else if(modifier == Result.defended){
            if(damage ==1){
                return "[Placeholder: Angel Assists]";
            }else{
                return "[Placeholder: Reyka Assists]";
            }
        }else{
            return self.name()+" takes a deep breath and you feel a haze of energy radiate out from her. An aura of pure arousal " +
                    "leaks out, so dense you can see it. As it hits you, you find yourself becoming supernaturally horny, while "+
                    self.name()+" looks much calmer.";
        }
    }
}
