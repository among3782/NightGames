package characters;

import Comments.SkillComment;
import combat.Result;
import daytime.Daytime;
import global.*;

import items.Clothing;
import items.Component;
import items.Toy;
import items.Trophy;

import java.util.ArrayList;
import java.util.HashSet;

import Comments.CommentGroup;
import Comments.CommentSituation;
import scenes.SceneFlag;
import scenes.SceneManager;
import skills.*;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Tag;
import status.Stsflag;

public class Reyka implements Personality {
	private static final long serialVersionUID = 8553663088141308399L;
	public NPC character;

	public Reyka() {
		this.character = new NPC("Reyka",ID.REYKA, 10, this);
		this.character.outfit[0].add(Clothing.lbustier);
		this.character.outfit[1].add(Clothing.lminiskirt);
		character.closet.add(Clothing.lbustier);
		character.closet.add(Clothing.lminiskirt);
		this.character.change(Modifier.normal);
		this.character.mod(Attribute.Dark, 12);
		this.character.mod(Attribute.Seduction, 12);
		this.character.mod(Attribute.Cunning, 2);
		this.character.mod(Attribute.Speed, 2);
		this.character.setUnderwear(Trophy.ReykaTrophy);
		this.character.getStamina().gainMax(15);
		this.character.getArousal().gainMax(60);
		this.character.getMojo().gainMax(45);
		character.add(Trait.female);
		character.add(Trait.succubus);
		character.add(Trait.darkpromises);
		character.add(Trait.greatkiss);
		character.add(Trait.tailed);
		character.add(Trait.Confident);
		Global.gainSkills(this.character);
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 5);
		character.strategy.put(Emotion.sneaking, 2);
		character.preferredSkills.add(Fuck.class);
		character.preferredSkills.add(Whisper.class);
		character.preferredSkills.add(Fly.class);
		character.preferredSkills.add(Fuck.class);
		character.preferredSkills.add(SpawnImp.class);
		character.preferredSkills.add(LustAura.class);
	}

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
		for(Skill a:available){
			if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Fuck")||a.toString().equalsIgnoreCase("Piston")||a.toString().equalsIgnoreCase("Tighten")||a.toString().equalsIgnoreCase("Ass Fuck")){
				mandatory.add(a);
			}
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&!c.hasModifier(Modifier.quiet)){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}	
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public void rest(int time, Daytime day) {
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2))&&character.money>=600&&character.getPure(Attribute.Seduction)>=20){
			character.gain(Toy.Strapon);
			character.money-=600;
		}
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Workshop");
		available.add("Play Video Games");
		for(int i=0;i<time;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		character.visit(3);
		if(Global.random(3)>1){
			character.gain(Component.Semen);
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.ReykaDWV, 1);
		}
	}

	@Override
	public String bbLiner() {
		switch(Global.random(2)){
		case 1:
			return "<i>\"That wasn't too hard, was it?  We better make sure everything still works properly!\"</i>";
		default:
			return "Reyka looks at you with a pang of regret: <i>\"In hindsight, damaging"
				+ " the source of my meal might not have been the best idea...\"</i>";
		}
	}

	@Override
	public String nakedLiner() {
		return "<i>\"You could have just asked, you know.\"</i> As you gaze upon her naked form,"
				+ " noticing the radiant ruby adorning her bellybutton, you feel"
				+ " sorely tempted to just give in to your desires. The hungry look"
				+ " on her face as she licks her lips, though, quickly dissuades you"
				+ " from doing so";
	}

	@Override
	public String stunLiner() {
		return "Reyka is laying on the floor, her wings spread out behind her,"
				+ " panting for breath";
	}

	@Override
	public String taunt() {
		return "<i>\"You look like you will taste nice. Maybe if let me have "
				+ "a taste, I will be nice to you too\"</i>";
	}

	@Override
	public void victory(Combat c, Tag flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if (opponent.hasDick())
			character.gain(Component.Semen);
		character.arousal.empty();
		if(opponent.human()) {
			if (flag == Result.anal) {
				Global.modCounter(Flag.PlayerAssLosses, 1);
				SceneManager.play(SceneFlag.ReykaPeggingVictory);
			} else if (opponent.is(Stsflag.enthralled)) {
				SceneManager.play(SceneFlag.ReykaEntralledVictory);
			} else if (character.pet != null && character.has(Trait.royalguard)) {
				SceneManager.play(SceneFlag.ReykaImpVictory);
			} else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ReykaSexVictory);
			} else if (character.has(Trait.limitedpotential)) {
				SceneManager.play(SceneFlag.ReykaFrustratedVictory);
			} else {
				SceneManager.play(SceneFlag.ReykaForeplayVictory);
			}
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		character.arousal.empty();
		Character opponent=c.getOther(character);
		if(opponent.human()) {
			declareGrudge(opponent, c);
			if (flag == Result.anal && c.stance.sub(character)) {
				SceneManager.play(SceneFlag.ReykaAnalDefeat);
			} else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ReykaSexDefeat);
			} else if (character.has(Trait.limitedpotential)) {
				SceneManager.play(SceneFlag.ReykaForeplayDefeatAlt);
			} else {
				SceneManager.play(SceneFlag.ReykaForeplayDefeat);
			}
		}
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if (target.human()) {
			return "<i>\"How kind of you to hold him for me, dear.\"</i> Reyka bows her head ever so slightly towards "+assist.name()+" and then turns her gaze upon you prone form. " +
					"She pulls a blindfold out of a small pocket in her miniskirt and secures it tightly over your eyes. <i>\"Wouldn't want to spoil the surprise, would we?\"</i> For " +
					"just a moment, you feel a slight pull on you mind, but the sensation passes quickly, replaced by that of one of her slender fingers invading your mouth. " +
					"It is covered with a fragrant liquid and given what you already know about her, there is little doubt in your mind of its origins. Your suspicions " +
					"are proven correct when the aphrodisiac reaches your loins, which respond as expected.<p>"
					+ "Apparently not one to stand on ceremony, Reyka immediately " +
					"settles over your now rock hard dick and lowers herself down onto it. The sensation is beyond comparison, her pussy wiggles and twists around you almost " +
					"as if it has a mind of its own, a mind connected to your own, knowing what will bring you the most pleasure. Your experiences in sex-fighting have " +
					"left you with impressive sexual stamina, but in the face of a succubus' unimpeded attentions, no man can hope to last. All too quickly you succumb " +
					"to the feelings, pouring your seed into the succubus. Just your seed. You were expecting her to take so much more, but you just feel a little tired, " +
					"not more so than after a regular orgasm.<p>"
					+ "The mystery is unveiled when Reyka removes the blindfold with her left hand. In her right hand, she is " +
					"holding a bottle. That bottle is firmly planted against the head of your still twitching dick and filled with your cum. <i>\"You looked scrumptious, " +
					"sitting there all helpless, but I was really in need of some supplies. Still, I didn't want to deny you the pleasure, so I crafted a teeny tiny " +
					"illusion just for you.\"</i> As she says this, she pours a small drop of your semen onto her finger and licks it up. <i>\"Yum, I might just have to find " +
					"you again later.\"</i> Both she and "+assist.name()+" walk off, in opposite directions, the former holding your clothes and the latter quietly giggling at " +
					"your embarrassment. Ah, well.";
		}
		else if(target.hasDick()){
			return "At the sight of "+target.name()+"'s erect cock, Reyka wraps her soft hands around it, slowly jerking up and down. "
					+ "Not seeing the reaction she wants, the succubus starts to fondle her breasts, arousing her prey. "
					+ "<i>\"You like that?\"</i> she asks, exposing her breasts and teasing the tip of her dick with her fingertip. "
					+ "Hungering for semen, she licks "+target.name()+"'s glans with her long tongue, enticing her hole. <p>"
					+ "Enjoying the expression on her face, Reyka starts to suck down on her manliness, welcoming her into her "
					+ "mouth. Feeling her hips start to move, the succubus begins to deep throat, allowing "+target.name()+"'s dick to "
					+ "reach the deepest parts of her throat. Feeling her dick starting to twitch a bit, she stops for a "
					+ "brief moment to catch air and prolong her orgasm. With her dick slipping out of her saliva-"
					+ "dripping mouth, the succubus manages to mutter out a sentence while slowly fondling her balls, "
					+ "<i>\"I've tasted better, but you're not so bad either, let's do something that'll feel even better...\"</i><p>"
					+ "Seeing that her prey's starting to emit pre-cum, the succubus decides it's time to heat things up "
					+ "a bit. She stands up and removes all her clothing, spreading her pussy dominantly.. <i>\"You "
					+ "think you can handle this?\"</i> she says standing over her lustful prey's wet cock.<p>"
					+ "Reyka grips "+target.name()+"'s dick in a swift motion and holds it at the edge of her hole, "
					+ "<i>\"Let's see how long you'll last.\"</i> The succubus gives a faint smile while drilling her opponent's penis into her deepest cavities, pleasuring them both. "
					+ "<p>"
					+ "The succubus rides "+target.name()+"'s dick roughly against her insides, making sure to finish her off quickly. "
					+ "At the critical moment, she quickly stops to exit and swallow her load. Cum fills her "
					+ "mouth, dripping down her throat and chin. She swallows everything in a single gulp.<p>"
					+ "Smiling at you, the succubus says in a devilish manner, <i>\"Thanks for helping, but I'm not quite "
					+ "feeling satisfied yet, mind helping me out again?\"</i>";
		}
		return "<i>\"My my, what a cute little offering you have caught for me tonight\"</i>, Reyka says, looking you at you with a satisfied grin on her face. <i>\"Not very nutritious, " +
				"but certainly a good deal of fun.\"</i> With that, she starts gently undressing "+target.name()+". When she is finished she squats down in front of her, bringing " +
				"her tail up between them. <i>\"Where would you prefer it dear?\"</i>, she asks "+target.name()+", whose eyes grow wide in shock. She manages to stammer out a " +
				"few syllables, but nothing quite coherent. \"No preference? Then I guess I will simply choose for you\" She brings her spade-tipped tail between "+target.name()+"'s " +
				"legs and starts running the very tip rapidly across her labia. When it is sufficiently wet, she moves it slightly upwards and moves it briskly back and forth over " +
				target.name()+"'s clit.<p>"+target.name()+", at first scared, now has her eyes closed and begins moaning feverishly. Just when she has almost reached her climax, " +
				"Reyka digs her tail deep into "+target.name()+"'s drooling pussy. This sends "+target.name()+" loudly over the edge. Her screams of pleasure are almost deafening, " +
				"and you have to work really hard to restrain her convulsing body. After a minute or so, the orgasm subsides and "+target.name()+" falls asleep and you gently lay her " +
				"down. When you turn to look at "+target.name()+", you are startled by the predatory look in her eyes. <i>\"I'm afraid all the excitement has left me a tad peckish. Be a " +
				"dear and help me out with that, will you?\"</i> You ponder whether or not you made a mistake in helping her.";
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if (target.human())
			return "Your fight with "
					+ assist.name()
					+ " starts out poorly; she already"
					+ " has you naked and aroused, whereas she seems as cool and calm as when"
					+ " you started. You haven't lost yet though, you just need to find an opening " +
					"and turn things around. A noise behind you causes you to turn and your vision is " +
					"filled with two piercing red eyes. <i>\"Kneel.\"</i> You drop to your knees involuntarily. " +
					"The rational part of your brain is telling you that Reyka is trying to dominate your " +
					"mind and you should resist, but what's the point? Reyka's tail binds your wrists and " +
					"she forces you to turn back to a bewildered "+assist.name()+". <i>\"I'm not poaching " +
					"your prey,\"</i> you hear her say. <i>\"He's all yours.\"</i>";

		return "Your fight with "
				+ target.name
				+ " starts out poorly; she already"
				+ " has you naked and aroused, whereas she seems as cool and calm as when"
				+ " you started. Fortune, though, seems to have a strange sense of humor as"
				+ " your salvation comes in the form of a winged demon swooping down on "
				+ target.name
				+ ". The two are briefly entangled in a ball of limbs and wings,"
				+ " but soon Reyka comes out on top. She is pinning "
				+ target.name
				+ " helplessly to the ground, holding her arms behind her back and"
				+ " locking her shoulders in place with her wings. The struggle has "
				+ "left " + target.name
				+ " completely naked and ready for you to"
				+ " take advantage of.";
	}
	
	public void watched(Combat c, Character target, Character viewer){
		if(viewer.human()) {
			SceneManager.play(SceneFlag.ReykaWatch, target);
		}
	}

	@Override
	public String describe() {
		return "Reyka the succubus stands before you, six feet tall with"
				+ " the most stunningly beautiful body you have ever seen."
				+ " Her long black hair enshrines her perfect face like a priceless"
				+ " painting, her breasts are large - yet not overly so -"
				+ " and impossibly firm. Her arms are slim and end in long-fingered,"
				+ " soft hands, nails polished shining red. Underneath, her long and"
				+ " perfectly formed legs and delicate feet stand in an imposing posture."
				+ " Behind her, you see a long, arrow tipped tail slowly waving around, as"
				+ " well as a pair of relatively small but powerful-looking bat wings.<br>"
				+ " Her gaze speaks of indescribable pleasure, but your mind reminds you"
				+ " of the cost of indulging in a succubus' body: Give her half a chance"
				+ " and she will suck out your very soul.";
	}

	@Override
	public void draw(Combat c, Tag flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(opponent.human()) {
			if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ReykaSexDraw);
			} else {
				SceneManager.play(SceneFlag.ReykaForeplayDraw);
			}
		}
	}
	
	@Override
	public boolean fightFlight(Character paramCharacter) {
		return !this.character.nude() || Global.random(3) == 1;
	}

	@Override
	public boolean attack(Character paramCharacter) {
		return !this.character.nude() || Global.random(3) == 1;
	}

	@Override
	public void ding() {
		if(character.getLevel() >= 30){
			int rand = Global.random(3);
			if (rand == 0) {
				this.character.mod(Attribute.Power, 1);
			} else if (rand == 1) {
				this.character.mod(Attribute.Seduction, 1);
			} else if (rand == 2) {
				this.character.mod(Attribute.Cunning, 1);
			}
			character.getStamina().gainMax(2);
			character.getArousal().gainMax(3);
		}
		else{
			this.character.mod(Attribute.Dark, 1);
			for (int i = 0; i < (Global.random(3)/2)+1; i++) {
				int rand = Global.random(3);
				if (rand == 0) {
					this.character.mod(Attribute.Power, 1);
				} else if (rand == 1) {
					this.character.mod(Attribute.Seduction, 1);
				} else if (rand == 2) {
					this.character.mod(Attribute.Cunning, 1);
				}
			}
			character.getStamina().gainMax(4);
			character.getArousal().gainMax(6);
		}
	}

	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case enthralling:
				return "As you approach Reyka, her eyes suddenly flash bright red and you find yourself unable to look away. She slowly, deliberately "
						+ "approaches you and firmly grabs your crotch.<br>"
						+ "<i>\"Sometimes I think you underestimate me, just because you make me feel good. You're just a human, don't forget your place.\"</i> "
						+ "Against your will, you find yourself nodding to her. Her dark power and personality are too much for you to overcome "
						+ "with willpower alone.<p>"
						+ "She smiles, satisfied, and releases her grip on you. <i>\"If you understand, then we can begin.\"</i>";
			case succubusvagina:
				return "As you and Reyka face off, she grins wickedly at you and flashes her pussy. <i>\"Did you enjoy fucking me last time? I never thought a "
						+ "sexfighter would willingly stick his dick in a succubus, so I wasn't ready. Now that I know how much you want to cum inside me, "
						+ "I'll show you my favorite way to drain a man dry.\"</i>";
			case darkness:
				return "As you prepare to fight Reyka, you suddenly freeze in place. Something is different about her. She's radiating an intense, malicious "
						+ "aura. What the hell happened to her?<p>"
						+ "<i>\"To me? Nothing.\"</i> Reyka extends her wings in an intimidating manner. <i>\"Did the human forget I was a demon? Just because the "
						+ "cheeky human made me cum first, he forgot I'm the daughter of one of the most prestigious infernal families? Maybe that's what happened "
						+ "to me. What do you think is about to happen to that cheeky little human?\"</i><p>"
						+ "Oh shit, she's really holding a grudge over her last loss. She'll be going all out this time.";
			default:
				break;		
			}
		}
		if(character.nude()){
			return "Reyka coyly covers her naked body with her wings. <i>\"Don't you have any decency? Shouldn't you look away from an undressed lady?\"</i>"
					+ "She laughs and folds her wings behind her, exposing herself. <i>\"I guess you can't take your eyes off me. Fortunately, I'm not easily embarrassed.\"</i>";
		}
		if(opponent.pantsless()){
			return "Reyka's eyes focus on your exposed groin. <i>\"What a delicious looking cock you have. I'd like to taste that a couple "
					+ "of different ways. What do you say?\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Reyka gives you a smile that's more pleasant than predatory. <i>\"You're quickly becoming my favorite human. My prey isn't "
					+ "usually such good company.\"</i> She licks her lips, and you see a dangerous glint in her eyes. <i>\"Of course, that makes "
					+ "your seed even tastier. Don't blame me if I get a little carried away.\"</i>";
		}

		return "<i>\"Yum, I was just looking for a tasty little morsel.\"</i><p>"
				+ "Reyka strikes a seductive pose and the devilish smile"
				+ " on her face reveals just what, or more specifically,"
				+ " who she intends that morsel to be.";
	}

	@Override
	public boolean fit() {
		return (!this.character.nude() || Global.random(3) == 1)
				&& (this.character.getStamina().percent() >= 50)
				&& (this.character.getArousal().percent() <= 50);
	}

	@Override
	public boolean night() {
		SceneManager.play(SceneFlag.ReykaAfterMatch);
		return true;
	}

	@Override
	public void advance(int rank) {
		if(rank >= 4 && !character.has(Trait.speeddemon)){
			character.add(Trait.speeddemon);
		}
	    if(rank >= 3 && !character.has(Trait.limitedpotential)){
	        character.add(Trait.limitedpotential);
        }
		if(rank >= 3 && !character.has(Trait.infernalexertion)){
			character.add(Trait.infernalexertion);
		}
		if(rank >= 2 && !character.has(Trait.royalguard)){
			character.add(Trait.royalguard);
		}
		
	}
	@Override
	public NPC getCharacter() {
		return character;
	}
	
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case dominant:
			return value>=25;
		case nervous:
			return value>=80;
		default:
			return value>=50;
		}
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case dominant:
			return 1.2f;
		case nervous:
			return .7f;
		default:
			return 1f;
		}
	}

	@Override
	public String image() {
		return "assets/reyka_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Ah, yes! Give me more!\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"I can't lose! Not like this!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Are you regretting fucking me now? It's time for you to cum now.\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Damn it! How did you get this good?!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"Thought my ass would be harmless, did you?\"</i>");
		comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Mmmm. I wonder if you'll cum right away if I put it in. Shall we find out?\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_WIN, "<i>\"Hah! Even on top it's hopeless! Now fuck me and finish it!\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_LOSE, "<i>\"Please... At least feed me...\"</i>");
		comments.put(CommentSituation.OTHER_ENTHRALLED, "<i>\"You're a good little slave, aren't you?\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"You really just can't resist me, can you?\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Reyka covers her pussy and moans deeply, though it is impossible to tell if it is from pain or pleasure.");
		comments.put(new SkillComment(Attribute.Arcane,true),"<i>\"That fae magic doesn't compare to demonic power.\"</i>");
		comments.put(new SkillComment(Attribute.Dark,true),"<i>\"You dare try to match a demon with dark power?.\"</i>");
		comments.put(new SkillComment(SkillTag.PET,false),"<i>\"Servant, fight for your mistress!\"</i>");

		return comments;
	}

	@Override
	public CommentGroup getResponses() {
		CommentGroup comments = new CommentGroup();
		return comments;

	}

	@Override
	public int getCostumeSet() {
		return 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if(character.getGrudge()==Trait.darkness || character.getGrudge() == Trait.succubusvagina){
			character.addGrudge(opponent, Trait.enthralling);
		}else if(c.eval(character)== Result.intercourse){
			character.addGrudge(opponent, Trait.succubusvagina);
		}else{
			character.addGrudge(opponent, Trait.darkness);
		}
		
	}
    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[1].clear();
        this.character.outfit[0].add(Clothing.lbustier);
        this.character.outfit[1].add(Clothing.lminiskirt);
    }
}
