package scenes;

import global.Global;
import characters.Character;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Scene {
    private List<String> text;
    private List<List<String>> images;

    public Scene(){
        this.text = new ArrayList<String>();
        this.images = new ArrayList<List<String>>();
    }
    public Scene(String text){
        this();
        this.text.add(text);
    }
    public Scene(String text, List<List<String>> images){
        this(text);
        images.addAll(images);
    }

    public Scene(List<String> text){
        this();
        this.text.addAll(text);
    }

    public Scene(List<String> text, List<List<String>> images){
        this(text);
        images.addAll(images);
    }

    public void addText(String text){
        this.text.add(text);
    }

    public void addImage(String path,String author){
        this.images.add(Arrays.asList(path,author));
    }

    public int getPages(){
        return Math.min(1,text.size()-images.size());
    }

    public void play(int page){
        int index = page + Math.min(page,images.size());
        if(text.size()>index) {
            Global.gui().message(text.get(index));
            if(images.size()>page){
                Global.gui().displayImage(images.get(page).get(0),images.get(page).get(1));
            }
            index++;
            if(text.size()>index) {
                Global.gui().message(text.get(index));
            }
        }else{
            Global.gui().message("Error in scene: page "+index+" of "+text.size()+" requested.");
        }
    }

    public void parse(Character player){
        parse(player,null);
    }

    public void parse(Character player, Character npc){
        parse(player,npc,null);
    }

    public void parse(Character player, Character npc, Character npc2){
        List<String> parsed = new ArrayList<String>();
        for(String raw: text){
            raw = raw.replaceAll("\\[PLAYER\\]",player.name());
            raw = raw.replaceAll("\\[Player\\]",player.name());
            if(npc != null) {
                raw = raw.replaceAll("\\[NPC\\]", npc.name());
                raw = raw.replaceAll("\\[OPPONENT\\]", npc.name());
                raw = raw.replaceAll("\\[heshe\\]", npc.pronounSubject(false));
                raw = raw.replaceAll("\\[HeShe\\]", npc.pronounSubject(true));
                raw = raw.replaceAll("\\[hisher\\]", npc.pronounSubject(false));
                raw = raw.replaceAll("\\[HisHer\\]", npc.pronounSubject(true));
                raw = raw.replaceAll("\\[himher\\]", npc.pronounTarget(false));
                raw = raw.replaceAll("\\[HimHer\\]", npc.pronounTarget(true));
            }
            if(npc2 != null) {
                raw = raw.replaceAll("\\[NPC2\\]", npc2.name());
                raw = raw.replaceAll("\\[heshe2\\]", npc2.pronounSubject(false));
                raw = raw.replaceAll("\\[HeShe2\\]", npc2.pronounSubject(true));
                raw = raw.replaceAll("\\[hisher2\\]", npc2.pronounSubject(false));
                raw = raw.replaceAll("\\[HisHer2\\]", npc2.pronounSubject(true));
                raw = raw.replaceAll("\\[himher2\\]", npc2.pronounTarget(false));
                raw = raw.replaceAll("\\[HimHer2\\]", npc2.pronounTarget(true));
            }
            raw = raw.replaceAll(" \\\""," <i>\\\"");
            raw = raw.replaceAll("\\\" ","\\\"</i> ");
            raw = raw.replaceAll("<p>\\\"","<p><i>\\\"");
            raw = raw.replaceAll("\\\"<p>","\\\"</i><p>");
            raw = raw.replaceAll("\\n","<p>");
            raw = raw.replaceAll("“","<i>\\\"");
            raw = raw.replaceAll("”","\\\"</i>");
            raw = raw.replaceAll("…","...");
            parsed.add(raw);
        }
        text = parsed;
    }

}
