package scenes;

import java.util.ArrayList;
import java.util.HashMap;

import characters.*;
import characters.Character;
import global.Flag;
import global.Global;
import global.Roster;
import global.SaveManager;
import gui.KeyableButton;
import gui.SceneButton;

public class YuiEvent implements Event {
	private int scene;
	private Character player;
	
	public YuiEvent(Character player){
		this.player = player;
		scene = 1;
	}

	@Override
	public void respond(String response) {
		if(response.startsWith("Take her virginity")){
			play("VirginitySex");
		}else if(response.startsWith("Let her service you")){
			play("VirginityHandjob");
		}else if(response.startsWith("Focus on her")){
			play("VirginityOral");
		}
	}

	@Override
	public boolean play(String response) {
		Dummy sprite = new Dummy("Yui",1,false);
		sprite.setBlush(2);
		sprite.setMood(Emotion.confident);
		Global.gui().clearCommand();
		if(response.startsWith("VirginityUndressing")){			
			Global.gui().loadPortrait(player, sprite);
			SceneManager.play(SceneFlag.YuiFirstUndress);
			Global.gui().choose("Let her service you");
			Global.gui().choose("Focus on her");
			return true;
		}else if(response.startsWith("VirginityHandjob")){
			sprite.setBlush(3);
			sprite.setMood(Emotion.dominant);
			Global.gui().loadPortrait(player, sprite);
			SceneManager.play(SceneFlag.YuiFirstService);
			Global.gui().choose("Take her virginity");
			return true;
		}else if(response.startsWith("VirginityOral")){
			sprite.setBlush(3);
			sprite.setMood(Emotion.desperate);
			Global.gui().loadPortrait(player, sprite);
			SceneManager.play(SceneFlag.YuiFirstLick);
			Global.gui().choose("Take her virginity");
			return true;
		}else if(response.startsWith("VirginitySex")){
			sprite.setBlush(3);
			Global.gui().loadPortrait(player, sprite);
			SceneManager.play(SceneFlag.YuiFirstSex);

			if(Global.checkFlag(Flag.autosave)){
				SaveManager.save(true);
			}
			Global.flag(Flag.Yui);
			Roster.scaleOpponent(ID.YUI);
			Global.gui().message("<b>You gained affection with Yui.</b>");
			Global.gui().endMatch();
			Roster.gainAffection(ID.PLAYER, ID.YUI, 3);
			return true;
		}
		return false;
	}

	@Override
	public String morning() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String mandatory() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		// TODO Auto-generated method stub
		
	}

}
