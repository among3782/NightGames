package trap;

import characters.Pool;
import global.Scheduler;
import items.Component;
import items.Consumable;
import items.Item;
import global.Global;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Encounter;

public class StripMine extends Trap{
    public StripMine(){
        super("Strip Mine","A high-tech trap that will destroy a layer of clothing",
                "Using the techniques Jett showed you, you rig up a one-time-use clothing destruction device.",3);
        recipe.put(Component.Tripwire,1);

        cost.put(Pool.BATTERY,3);
    }

	@Override
	public void trigger(Character target) {
		if(target.human()){
			if(target.nude()){
				Global.gui().message("You're momentarily blinded by a bright flash of light. A camera flash maybe? Is someone taking naked pictures of you?");
			}
			else{
				Global.gui().message("You're suddenly dazzled by a bright flash of light. As you recover from your disorientation, you notice that it feel a bit drafty. " +
						"You find you're missing some clothes. You reflect that your clothing expenses have gone up significantly since you joined the Games.");
			}
		}
		else if(target.location().humanPresent()){
			Global.gui().message("You're startled by a flash of light not far away. Standing there is a half-naked "+target.name()+", looking surprised.");
		}
		if(!target.top.isEmpty()){
			target.shred(Character.OUTFITTOP);
		}
		if(!target.bottom.isEmpty()){
			target.shred(Character.OUTFITBOTTOM);
		}
		target.location().opportunity(target,this);
	}

	@Override
	public boolean requirements(Character owner) {
		return owner.getPure(Attribute.Science)>=4;
	}

    @Override
    public Trap copy() {
        return new StripMine();
    }

}
