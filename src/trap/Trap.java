package trap;
import areas.Area;
import areas.Deployable;
import areas.Location;
import characters.Pool;
import combat.Combat;
import combat.Encounter;

import characters.Character;
import global.Global;
import global.Scheduler;
import items.Item;
import status.Flatfooted;

import java.util.HashMap;

public abstract class Trap implements Deployable{
	protected Character owner;
	protected Area location;
	private String label;
	private String description;
	private String setup;
	protected boolean decoy;
	private int priority;
	protected HashMap<Item,Integer> recipe;
	protected HashMap<Pool,Integer> cost;

	public Trap(String label, String description, String setup, int priority){
		this.label = label;
		this.description = description;
		this.setup = setup;
		this.decoy = false;
		this.priority = priority;
		recipe = new HashMap<Item, Integer>();
		cost = new HashMap<Pool,Integer>();
	}



	public boolean decoy(){
		return decoy;
	}

    public boolean recipe(Character owner){
        boolean affordable = true;
        for(Item i: recipe.keySet()){
            if(!owner.has(i,recipe.get(i))){
                affordable = false;
            }
        }
        for(Pool p: cost.keySet()){
			if(!owner.canSpend(p,cost.get(p))){
				affordable = false;
			}
		}
        return affordable;
    }

    public Trap place(Character owner, Area area){
        Trap copy = copy();
        copy.owner=owner;
        copy.location = area;
        for(Item i: recipe.keySet()){
            owner.consume(i, recipe.get(i));
        }
        for(Pool p:cost.keySet() ){
        	owner.spend(p,cost.get(p));
		}
        area.place(copy);
        return copy;
    }

    public abstract void trigger(Character target);

	public abstract boolean requirements(Character owner);
	public abstract Trap copy();
	public String setup(){
	    return setup;
    }

	public String description(){
		return description;
	}
	public Character owner(){
		return owner;
	}
	@Override
	public void resolve(Character active) {
		if(active!=owner){
			trigger(active);
		}
	}
    @Override
    public int priority() {
        return priority;
    }

	public String toString(){
		return label;
	}

    public void capitalize(Character attacker, Character victim, Encounter enc) {
        Combat c;
        if(attacker.human() || victim.human()) {
			c = Global.gui().beginCombat(attacker, victim);
		}else{
        	c = Scheduler.getMatch().buildCombat(attacker,victim);
		}
		victim.add(new Flatfooted(victim,1));
        enc.engage(c);
        attacker.location().remove(this);
    }
}
