package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Stsflag;

public class CleanseMind extends Skill{

    public CleanseMind(Character self){
        super("Cleanse Mind",self);
        addTag(Attribute.Spirituality);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Spirituality)>=2;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.is(Stsflag.mindaffecting) && !self.is(Stsflag.stunned) &&self.canSpend(Pool.FOCUS,2);
    }

    @Override
    public String describe() {
        return "Remove all mind-affecting states: 2 Focus";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.FOCUS,2);
        if(self.human()){
            c.write(self,deal(0, Result.normal,target));
        }
        else if(target.human()){
            c.write(self,receive(0, Result.normal,target));
        }
        self.removeStatus(Stsflag.mindaffecting,c);
        self.emote(Emotion.confident,100);

    }

    @Override
    public Skill copy(Character user) {
        return new CleanseMind(user);
    }

    @Override
    public Tactics type() {
        return Tactics.recovery;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "Something deep in your soul rings like an alarm clock, snapping your mind from its haze.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return self.name()+"'s eyes suddenly regain clarity. She seems to have cleansed all distortion from her mind.";
    }
}
