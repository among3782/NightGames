package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Horny extends Status {

	public Horny(Character affected, int magnitude, int duration) {
		super("Horny",affected);
		this.duration=duration;
		this.magnitude = magnitude;
		lingering = true;
		stacking = true;
		flag(Stsflag.horny);
		tooltip = "Temptation damage each turn";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}
		this.affected = affected;
		
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your heart pounds in your chest as you try to suppress your arousal.";
		}
		else{
			return affected.name()+" is flushed and her nipples are noticeably hard.";
		}
	}


	@Override
	public Status copy(Character target) {
		return new Horny(target,magnitude,duration);
	}
	@Override
	public void turn(Combat c) {
		affected.tempt(magnitude);
		affected.emote(Emotion.horny,20);
		decay(c);
	}
}
