package scenes;

public enum SceneType {
    VICTORY,
    DEFEAT,
    DRAW,
    INTERVENTION,
    DAYTIME,
    EVENT,
    THREESOME,
}
