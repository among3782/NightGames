package trap;

import areas.Area;
import areas.Location;
import global.Scheduler;
import items.Component;
import items.Flask;
import items.Item;
import status.Flatfooted;
import global.Global;
import combat.Combat;
import combat.Encounter;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class AphrodisiacTrap extends Trap {
	public AphrodisiacTrap(){
	    super("Aphrodisiac Trap", "Sprays an aphrodisiac to arouse whoever trips it","You set up a spray trap to coat an unwary opponent in powerful aphrodisiac.",3);
	    recipe.put(Flask.Aphrodisiac,1);
	    recipe.put(Component.Tripwire,1);
	    recipe.put(Component.Sprayer,1);
    }
    @Override
	public void trigger(Character target) {
		if(!target.check(Attribute.Perception, 15-(target.get(Attribute.Perception)+target.bonusDisarm()))){
			if(target.human()){
				Global.gui().message("You spot a liquid spray trap in time to avoid setting it off. You carefully manage to disarm the trap and pocket the potion.");
				target.gain(Flask.Aphrodisiac);
				target.location().remove(this);
			}
		}
		else{
			if(target.human()){
				Global.gui().message("There's a sudden spray of gas in your face and the room seems to get much hotter. Your dick goes rock-hard and you realize you've been " +
						"hit with an aphrodisiac.");
			}
			else if(target.location().humanPresent()){
				Global.gui().message(target.name()+" is caught in your trap and sprayed with aphrodisiac. She flushes bright red and presses a hand against her crotch. It seems like " +
						"she'll start masturbating even if you don't do anything.");
			}
			target.tempt(40);
			target.location().opportunity(target,this);
		}
	}

	@Override
	public boolean requirements(Character owner) {
		return owner.getPure(Attribute.Cunning)>=14 && !owner.has(Trait.direct) &&!owner.has(Trait.sportsmanship);
	}

    @Override
    public Trap copy() {
        return new AphrodisiacTrap();
    }

}
