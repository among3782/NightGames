package characters;

import java.io.Serializable;

public class Meter implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private int current;
	private int max;

	public Meter(int max){
		this.max=max;
		this.current=0;
	}
	public int get(){
		return current;
	}
	public int max(){
		return max;
	}
	public void reduce(int i){
		if(i>0){
			current-=i;
		}
		if(current<0){
			current=0;
		}
	}
	public int minimize(int i){
		int mag = i;
		if(mag>0){
			if(mag>=current){
				mag = current-1;
			}
			current-=mag;
		}
		return mag;
	}
	public void restore(int i){
		current+=i;
		if(current>max()){
			current=max();
		}
	}
	public int edge(int i){
		int mag = i;
		if( current + mag >= max){
			mag = max - (current +1);
		}
		current+=mag;
		return mag;
	}
	public int addPercent(int perc){
		int x = (perc*max())/100;
		restore(x);
		return x;
	}
	public boolean isEmpty(){
		return current<=0;
	}
	public boolean isFull(){
		return current==max();
	}
	public void empty(){
		current=0;
	}
	public void fill(){
		current=max();
	}
	public void set(int i){
		current=i;
		if(current>max()){
			current=max();
		}
	}
	public void gainMax(int i){
		max+=i;
		if(current>max()){
			current=max();
		}
	}
	public void setMax(int i){
		max=i;
		current=max;
	}
	public int percent(){
		return 100*current/max();
	}
	public Meter clone() throws CloneNotSupportedException {
		return (Meter) super.clone();
	}
}
