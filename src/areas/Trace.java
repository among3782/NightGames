package areas;

import actions.Movement;
import characters.Character;
import global.Constants;
import global.Global;

public class Trace {
    private Character person;
    private Movement event;
    private int time;

    public Trace(Character person, Movement event){
        this.person = person;
        this.event = event;
        this.time = 0;
    }

    public boolean age(){
        time++;
        return time >= 12;
    }

    public Character getPerson(){ return person; }

    public String track(int tracking){
        int score = tracking - difficulty();
        if(score>0){
            return getIdentity(score)+getAct(score)+getTime(score);
        }else{
            return "";
        }
    }

    private String getIdentity(int score){
        if(Global.random(score)>=5){
            return person.name();
        }else{
            return "Someone";
        }
    }
    private String getAct(int score){
        if(Global.random(score)>=5){
            return event.traceDesc();
        }else{
            return " was here ";
        }
    }

    private String getTime(int score){
        if(Global.random(score)>=10){
            if(time > 6){
                return "more than 30 minutes ago.";
            }else{
                return "within the past "+(time*5)+" minutes.";
            }
        }else if(Global.random(score)>=5){
            if(time > 6){
                return "a while ago.";
            }else{
                return "recently.";
            }

        }else{
            return "at some point.";
        }
    }

    private int difficulty(){
        return Constants.BASETRACKDIFF +time*5;
    }


}
