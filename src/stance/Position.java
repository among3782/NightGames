package stance;
import characters.Anatomy;
import characters.Character;
import java.io.Serializable;

import combat.Combat;



public abstract class Position implements Serializable, Cloneable{
	public Character top;
	public Character bottom;
	public int time;
	public Stance en;
	protected int pace;
	public int strength;
	protected boolean contact;
	protected boolean submission;
	protected boolean pleasure;
	
	public Position(Character top, Character bottom, Stance stance){
		this.top=top;
		this.bottom=bottom;
		this.en=stance;
		time=0;
		pace=0;
		strength=0;
		contact = true;
		submission = false;
		pleasure = false;
	}
	public void decay(){
		time++;
	}
	public void checkOngoing(Combat c){
		return;
	}
	public boolean anal(){
		return this.en == Stance.anal || this.en == Stance.analm;
	}
	public boolean inContact(){ return contact; }
	public abstract String describe();
	public abstract boolean mobile(Character c);
	public abstract boolean kiss(Character c);
	public abstract boolean dom(Character c);
	public abstract boolean sub(Character c);
	public abstract boolean reachTop(Character c);
	public abstract boolean reachBottom(Character c);
	public abstract boolean prone(Character c);
	public abstract boolean feet(Character c);
	public abstract boolean oral(Character c);
	public abstract boolean behind(Character c);
	public abstract boolean penetration(Character c);
	public abstract Position insert(Character c);
	public Stance enumerate(){
		return this.en;
	}
	public void setPace(int speed){
		this.pace = speed;
	}
	public Position clone() throws CloneNotSupportedException {
	    return (Position) super.clone();
	}
	public void struggle(Combat c){
		if(submission){
			if(bottom.human()){
				c.write(top,"Your struggles are met with an even tighter grip on your balls.");
			}else if(top.human()){
				c.write(top,"You apply more pressure to "+bottom.name()+"'s genitals to stop her from struggling.");
			}
			bottom.pain(5, Anatomy.genitals,c);
		}
		if(pleasure){
			if(bottom.human()){
				c.write(top,"The friction from struggling stimulates your penis even more.");
			}else if(top.human()){
				c.write(top,bottom.name()+" grinds against your leg, trying to free herself.");
			}
			bottom.pleasure(5, Anatomy.genitals,c);
		}

	}
	public int escapeDC(Character dom, Character sub){
		return dom.bonusPin(strength)-((5*time)+sub.escape());
	}
}
