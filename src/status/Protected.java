package status;

import characters.Character;
import characters.Trait;

public class Protected extends Status{

    public Protected(Character affected){
        super("Mysterious Protection",affected);
        this.duration = 6;
        this.flag(Stsflag.protection);
        this.traits.add(Trait.painimmune);
        this.traits.add(Trait.weakimmune);
        tooltip = "Protection from Pain and Weaken damage";
    }

    @Override
    public String describe() {
        if(affected.human()){
            return "You are protected from attacks by a mysterious force.";
        }
        else{
            return affected.name()+" is protected by a mysterious force.";
        }
    }

    @Override
    public Status copy(Character target) {
        Status copy = new Protected(affected);
        copy.duration = duration;
        return copy;
    }
}
