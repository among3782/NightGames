package trap;

import characters.Character;
import combat.Encounter;

public class ClothesSnareTrap extends Trap {

    public ClothesSnareTrap(){
        super("Clothes Snare","","",3);
    }

    @Override
    public void trigger(Character target) {

    }

    @Override
    public boolean requirements(Character owner) {
        return false;
    }

    @Override
    public Trap copy() {
        return new ClothesSnareTrap();
    }


    @Override
    public void capitalize(Character attacker, Character victim, Encounter enc) {

    }
}
