package characters;

import Comments.SkillComment;
import combat.Result;
import daytime.Daytime;
import global.*;

import items.*;

import java.util.ArrayList;
import java.util.HashSet;

import Comments.CommentGroup;
import Comments.CommentSituation;
import scenes.SceneFlag;
import scenes.SceneManager;
import skills.*;
import stance.Stance;
import status.Hypersensitive;
import status.Oiled;

import combat.Combat;
import combat.Tag;

import actions.Action;
import actions.Movement;
import status.Stsflag;

public class Mara implements Personality {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3812726803607189573L;
	public NPC character;
	public Mara(){
		character = new NPC("Mara",ID.MARA,1,this);
		character.outfit[0].add(Clothing.bra);
		character.outfit[0].add(Clothing.Tshirt);
		character.outfit[1].add(Clothing.underwear);
		character.outfit[1].add(Clothing.shorts);
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.Tshirt);
		character.closet.add(Clothing.underwear);
		character.closet.add(Clothing.shorts);
		character.change(Modifier.normal);
		character.mod(Attribute.Cunning, 2);
		character.mod(Attribute.Perception, 2);
		character.gain(Consumable.ZipTie,10);
		character.gain(Flask.Lubricant,5);
		Global.gainSkills(character);
		character.add(Trait.female);
		character.add(Trait.petite);
		character.add(Trait.dexterous);
		character.add(Trait.ticklish);
		character.setUnderwear(Trophy.MaraTrophy);
		character.plan = Emotion.sneaking;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 2);
		character.strategy.put(Emotion.sneaking, 5);
		character.preferredSkills.add(Tie.class);
		character.preferredSkills.add(UseOnahole.class);
		character.preferredSkills.add(UseFlask.class);
		character.preferredSkills.add(WindUp.class);
		character.preferredSkills.add(MatterConverter.class);
		character.preferredSkills.add(Stomp.class);
		character.preferredSkills.add(SpawnSlime.class);
		character.preferredSkills.add(Footjob.class);
	}
	@Override
	public Skill act(HashSet<Skill> available,Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
        for(Skill a:available){
            if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Ass Fuck")){
                mandatory.add(a);
            }
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
        }
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&!c.hasModifier(Modifier.quiet)){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public void rest(int time, Daytime day) {
		if(character.rank>=1){
			if(character.money>0){
				day.visit("Workshop", character, Global.random(character.money));
			}
		}
		if(!(character.has(Toy.Onahole)||character.has(Toy.Onahole2))&&character.money>=300){
			character.gain(Toy.Onahole);
			character.money-=300;
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Crop)||character.has(Toy.Crop2))&&character.money>=200){
			character.gain(Toy.Crop);
			character.money-=200;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2))&&character.money>=600&&character.getPure(Attribute.Seduction)>=20){
			character.gain(Toy.Strapon);
			character.money-=600;
		}
		character.visit(2);
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		if(character.rank>0){
			available.add("Workshop");
			character.gain(Potion.SuperEnergyDrink);
		}
		available.add("Play Video Games");
		for(int i=0;i<time-2;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.MaraDWV, 1);
		}
	}

	@Override
	public String bbLiner() {
		if(character.getAffection(Global.getPlayer())>=25){
			return "Mara gives you a sympathetic look with just a hint of a grin. <i>\"Sorry, "+Global.getPlayer().name()+", but you know the drill by now. I've got to be "
					+ "cruel to be kind. I promise I'll make it up to you soon.\"</i>";
		}
		switch(Global.random(3)){
		case 1:
			return "<i>\"Bingo!  I think I got both of them that time!\"</i> Mara exclaims, looking proud of herself.";
		case 2:
			return "<i>\"You should probably put some ice on those balls tonight to keep them from swelling up.\"</i> "
					+ "Mara says, cracking a smug smile.";
		default:
			return "Mara gives you a look of not quite genuine concern. <i>\"That must have really hurt. Sorry for scrambling your eggs. I feel really bad about that. Also for " +
			"lying just now. I'm not actually that sorry.\"</i>";
		}
		
	}

	@Override
	public String nakedLiner() {
		return "Mara gives an exaggerated squeal and covers herself, but can't quite conceal her excited grin. <i>\"You fiend! How dare you strip a helpless, innocent girl like this?! "
				+ "Are you planning to do brutish and naughty things to my naked body?\"</i>";
	}

	@Override
	public String stunLiner() {
		return "Mara lets out a slightly pained whimper. <i>\"Go easy on me. I'm not really the masochistic type.\"</i>";
	}

	public String winningLiner() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String taunt() {
		return "<i>\"If you want me to get you off so badly,\"</i> Mara teases coyly. <i>\"You should have just said so from the start. You don't need to put up this token resistance.\"</i>";
	}

	@Override
	public void victory(Combat c, Tag flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(opponent.human()) {
			character.arousal.empty();
			if (flag == Result.anal) {
				Global.modCounter(Flag.PlayerAssLosses, 1);
				SceneManager.play(SceneFlag.MaraPeggingVictory);
			} else if (character.pet != null) {
				SceneManager.play(SceneFlag.MaraSlimeVictory);
			} else if (flag == Result.intercourse) {
				if (character.has(Toy.ShockGlove) && Global.random(2) == 0) {
					SceneManager.play(SceneFlag.MaraShockVictory);
				} else {
					SceneManager.play(SceneFlag.MaraSexVictory);
				}
			} else if (c.lastact(character).hasTag(Result.feet)) {
				SceneManager.play(SceneFlag.MaraFootjobVictory);
			} else if (character.has(Trait.madscientist) && character.has(Flask.Lubricant) && Global.random(2) == 0) {
				SceneManager.play(SceneFlag.MaraLubeVictory);
				opponent.add(new Oiled(opponent));
			} else if ((character.has(Toy.Onahole) || character.has(Toy.Onahole2)) && Global.random(2) == 0) {
				SceneManager.play(SceneFlag.MaraOnaholeVictory);
			} else {
				opponent.arousal.set(opponent.arousal.max() / 3);
				SceneManager.play(SceneFlag.MaraForeplayVictory);
			}
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(opponent.human()) {
			if (c.stance.sub(character) && (c.stance.enumerate() == Stance.pin || c.stance.enumerate() == Stance.reversepin)) {
				SceneManager.play(SceneFlag.MaraPinDefeat);
			}
			else if (c.stance.anal() && c.stance.sub(character)) {
				SceneManager.play(SceneFlag.MaraAnalDefeat);
			}
			else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.MaraSexDefeat);
			}
			else if (c.lastact(opponent).hasTag(SkillTag.TICKLE)) {
				SceneManager.play(SceneFlag.MaraTickleDefeat);
			}
			else if (opponent.getArousal().percent() < 30) {
				SceneManager.play(SceneFlag.MaraForeplayDefeatEasy);
			}
			else if (character.is(Stsflag.horny)) {
				SceneManager.play(SceneFlag.MaraHornyDefeat);
			}
			else if (character.is(Stsflag.bound)) {
				SceneManager.play(SceneFlag.MaraBoundDefeat);
			}
			else if (character.has(Trait.madscientist) && character.has(Flask.SPotion) && Global.random(2) == 0) {
				character.add(new Hypersensitive(character));
				SceneManager.play(SceneFlag.MaraForeplayDefeatSensitive);
			}
			else if (Global.random(2) == 0) {
				SceneManager.play(SceneFlag.MaraForeplayDefeatAlt);
			} else {
				SceneManager.play(SceneFlag.MaraForeplayDefeat);
			}
		}
	}

	@Override
	public String describe() {
	    if(character.getPure(Attribute.Temporal)>1){
	       return "Mara's science fiction look has reached a new level. Her assortment of thrown together inventions have been replaced by a sleek, futuristic body-suit. The high-tech gauntlet on her " +
                   "hand seems to be the culmination of all her inventions, but you have no idea how she packed them all into such a compact form. She looks a bit like a supervillain, albiet a " +
                   "very cute and petite one.";
        }
		else if(character.has(Trait.madscientist)){
			return "Mara has gone high tech. She has a rig of equipment on harnesses that seem carefully placed so as not to interfere with clothing removal. The glasses she's wearing appear to be " +
					"computerized rather than prescription. She also has a device of unknown purpose strapped to her arm. Underneath all of that, she has the same cute, mischievous expression she " +
					"you're used to.";
		}
		else{
			return "Mara is short and slender, with small but well shaped breasts. She has dark skin, and short, curly black hair. Her size and cute features make her look a few years " +
					"younger than she actually is, and she wears a near constant playful smile. She's far from physically intimidating, but her sharp eyes reveal her exceptional intellect.";
		}
	}

	@Override
	public void draw(Combat c, Tag flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(opponent.human()) {
			if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.MaraSexDraw);
			} else if (character.getPure(Attribute.Temporal) >= 1 && Global.random(2) == 0) {
				SceneManager.play(SceneFlag.MaraTemporalDraw);
			} else if (character.has(Trait.madscientist) && character.has(Flask.Aphrodisiac) && Global.random(2) == 0) {
				SceneManager.play(SceneFlag.MaraAphrodisiacDraw);
			} else {
				SceneManager.play(SceneFlag.MaraForeplayDraw);
			}
		}
	}
	@Override
	public boolean fightFlight(Character opponent) {
		return !character.nude()||opponent.nude();
	}
	@Override
	public boolean attack(Character opponent) {
		return true;
	}
	@Override
	public void ding() {
		if(character.getRank()>=4){
			character.mod(Attribute.Science, 1);
			character.mod(Attribute.Temporal, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(4);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
				else if(rand==3){
					character.mod(Attribute.Science, 1);
				}
			}
		}
		else if(character.getPure(Attribute.Science)>=1){
			character.mod(Attribute.Science, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(4);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
				else if(rand==3){
					character.mod(Attribute.Science, 1);
				}
			}
		}
		else{
			character.mod(Attribute.Cunning, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(3);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
			}
		}
		character.getStamina().gainMax(4);
		character.getArousal().gainMax(4);
		character.money += character.prize()*5;
	}
	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if(target.human()){
			return "Mara settles between your legs, holding your dick between her bare feet. Her soft, smooth soles begin to stroke the length of your shaft. She frequently " +
					"uses her toes to tease your balls and the head of your penis. As you start to leak pre-cum, she smears it all over your dick, using the lubricant to give " +
					"you a more intense footjob. Mindful of her own needs, she reaches between her own legs and starts rubbing her pussy and clit, giving you a sexy show. " +
					"For a moment, it crosses your mind that if you can hold out long enough, she may slip up and climax before you. The moment passes, however, when Mara " +
					"accelerates her stroking and you realize you won't be able to last more than a few more seconds. Sure enough, your jizz soon shoots into the air like " +
					"a fountain and paints her legs and feet. Mara continues to stimulate your oversensitized dick and balls while she finishes herself off, apparently too " +
					"caught up in her own enjoyment to notice your discomfort.";
		}
		else{
			if(target.hasDick()){
				return String.format("Mara approaches %s like a panther claiming its prey. She runs her fingers down the length of %s's body, eliciting a shiver " +
					"each time she hits a ticklish spot. Her probing fingers avoid %s's nipples and genitals, focusing instead on the ticklish areas under %s arms, " +
					"behind %s knees and on %s inner thighs. You struggle to hold onto %s as %s squirms and pleads for mercy. After a few minutes, %s pleas " +
					"shift in tone and you realise Mara's dancing fingers have moved to %s dick and balls. %s entire body trembles as if unable to decide whether it's being " +
					"tickled into submission or stroked to ejaculation. You finally hear a breathless gasp as %s hits %s climax and shudders in your arms. You release " +
					"%s and %s collapses, completely exhausted. Mara grins at you mischievously. <i>\"%s obviously enjoyed that. Do you want to be next?\"</i>",
					target.name(),target.name(),target.name(),target.possessive(false),target.possessive(false),target.possessive(false),target.name(),target.pronounSubject(false),
					target.possessive(false),target.possessive(false),target.possessive(false),target.name(),target.possessive(false),target.pronounTarget(false),target.pronounSubject(false),
					target.possessive(true));
			}else{
			return "Mara approaches "+target.name()+" like a panther claiming its prey. She runs her fingers down the length of "+target.name()+"'s body, eliciting a shiver " +
					"each time she hits a ticklish spot. Her probing fingers avoid "+target.name()+"'s nipples and pussy, focusing instead on the ticklish areas under her arms, " +
					"behind her knees and on her inner thighs. You struggle to hold onto "+target.name()+" as she squirms and pleads for mercy. After a few minutes, her pleas " +
					"shift in tone and you realise Mara's dancing fingers have moved to her pussy and clit. Her entire body trembles as if unable to decide whether it's being " +
					"tickled into submission or fingered to ecstasy. You finally hear a breathless gasp as "+target.name() +" hits her climax and shudders in your arms. You release " +
					"her and she collapses, completely exhausted. Mara grins at you mischievously. <i>\"She obviously enjoyed that. Do you want to be next?\"</i>";
			}
		}
	}
	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "You face off with "+assist.name()+", looking for any opening. Her eyes dart momentarily past you, but before you can decide if her distraction is " +
					"real or a feint, a small hand reaches between your legs and grabs your nutsack tightly. You can't get a good look at your attacker, clinging to your back, " +
					"but her small size and mocha skin give away Mara's identity. This information doesn't really help you much, as it's too late to defend yourself." +
					" She yanks on your jewels, forcing you to your knees. Both girls work to restrain your arms, but it's " +
					"not really necessary since Mara literally has you by the balls. She releases your abused jewels once the fight has left you and focuses on holding your arms, " +
					"while "+assist.name()+" moves to your front.<br>";
		}
		else{
			return "So far this hasn't been your proudest fight. "+target.name()+" was able to pin you early on and is currently rubbing your dick between her thighs. " +
					"You've almost given up hope of victory, until you spot Mara creeping up behind her. She seems thoroughly amused by your predicament and makes no " +
					"move to help you, despite being easily in reach. You give her your best puppy-dog eyes, silently pleading while trying not to give away her presence. " +
					"Mara lets you squirm a little longer before winking at you and tickling "+target.name()+" under her arms. "+target.name()+" lets out a startled yelp " +
					"and jumps in surprise. You use the moment of distraction to push her off balance and Mara immediately secures her arms.<br>";
		}
	}
	
	public void watched(Combat c, Character target, Character viewer){
		if (viewer.human()){
			if(character.has(Trait.madscientist)) {
				SceneManager.play(SceneFlag.MaraWatchScience,target);
			}else{
				SceneManager.play(SceneFlag.MaraWatch,target);	}
		}
	}
	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case perfectplan:
				if(opponent.nude()){
					return "Mara looks strangely disappointed as she looks over your naked body. It's kinda a blow to your ego. "
							+ "Mara shakes her head hurriedly as she sees your expression.<p>"
							+ "<i>\"Oh no, you look hot! I like the view. I just had a cool plan to strip you naked, and now it's "
							+ "going to waste.\"</i>";
				}else{
					return "Mara smiles confidently as she sees you. <i>\""+opponent.name()+", I knew I'd find you here.\"</i><p>"
							+ "She seems self-assured, but you know she's just bluffing to compensate for her previous loss. She couldn't "
							+ "have known where you'd be.<p>"
							+ "Mara shakes her hips in a sort of stationary swagger and points downward. <i>\"Oh no? Then what are you standing on?\"</i><p>"
							+ "You look down and see that you're standing on an 'X' marked in chalk. While your attention is focused downward, you're "
							+ "suddenly soaked from above. Your clothes rapidly dissolve away, leaving you completely naked. Looking up at the ceiling "
							+ "far too late, you see the overturned bucket that previously contained the solution.";
				}
			case inspired:
				return "Mara looks pretty upbeat despite her recent loss. Did something good happen to her since your last fight?<p>"
						+ "<i>\"Not really, I'm just feeling particularly inspired right now. I've got a dozen new ideas running through my head.\"</i><p>"
						+ "She grins and gives you a seductive wink. <i>\"Maybe getting an orgasm from a hot boy is my muse. Why don't you come by "
						+ "my lab next time I'm working so we can test that theory.\"</i>";
			case planB:
				return "For once, Mara looks too frustrated to hide with her normal cutesy facade. It's probably because you've dealt her consecutive losses.<p>"
						+ "<i>\"OK, maybe my last couple plans haven't ended up going too well. I'm not exactly the sturdiest fighter here. "
						+ "But this time I've prepared some emergency supplies in case things go bad. This time- this time- this time I'll beat you!\"</i>";
			case experimentalweaponry:
				return "Mara shows up out of breath, holding a bag. She didn't have it with her at the start of the match. Did she run off to retrieve it?<p>"
						+ "<i>\"I wasn't planning to use any of these before they were properly tested, but you're stronger than I thought.\"</i> She pulls "
						+ "several sex toys out of the bag. <i>\"I designed these to be way more effective than normal toys. I'm like 80% certain they "
						+ "aren't dangerous.\"</i>";
				
			default:
				break;
			}
		}
		if(opponent.pantsless()){
			return "Mara giggles and crouches down to get a closer look at your dick. <i>\"It's such a funny looking thing, but I really want to play with it.\"</i><p>"
					+ "She looks up at your face and gives you a cute wink. <i>\"I bet you'd like that too.\"</i>";
		}
		if(character.nude()){
			return "Mara giggles sheepishly as a mild blush darkens her cheeks. She doesn't cover her naked body, but you can tell she's a bit self-conscious.<p>"
					+ "<i>\"I don't suppose you'll let me go get dressed before we start? No? Well, next time I catch you naked, you better be prepared for some "
					+ "serious teasing.\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Mara signals a 'Time Out' and walks toward you without any sign of hostility. There's no 'Time Out' rule in the Games, so this could be "
					+ "a trap, but you relax and let her approach anyway.<p>"
					+ "She stands on her tiptoes and kisses you tenderly on the lips. Technically, that is a sneak attack, but it's a pretty weak one.<p>"
					+ "She smiles as she backs away. <i>\"I wasn't trying to turn you on. I just wanted to kiss my boy before we started. I may need "
					+ "to use some dirty tricks during the fight, but that should remind you how I feel about you.\"</i>";
			
		}
		if(character.has(Trait.madscientist)){
			return "Mara shows off the complex gadget on her arm. <i>\"I've got some new toys now. You aren't just up against me, "
					+ "you're up against the power of science!\"</i>";
		}
		return "Mara smiles and faces you, practically daring you to attack.";
	}
	@Override
	public boolean fit() {
		return character.getStamina().percent()>=75&&character.getArousal().percent()<=10&&!character.nude();
	}
	@Override
	public boolean night() {
		Global.gui().loadPortrait(Global.getPlayer(), this.character);
		SceneManager.play(SceneFlag.MaraAfterMatch);
		return true;
	}
	public void advance(int rank){
		if (rank >= 4 && character.getPure(Attribute.Temporal)==0){
			character.mod(Attribute.Temporal,1);
			character.outfit[Character.OUTFITTOP].removeAllElements();
			character.outfit[Character.OUTFITBOTTOM].removeAllElements();
			character.outfit[Character.OUTFITTOP].add(Clothing.nipplecover);
			character.outfit[Character.OUTFITTOP].add(Clothing.chestpiece);
			character.outfit[Character.OUTFITTOP].add(Clothing.techarmor);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.groincover);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.techpants);
			character.closet.add(Clothing.nipplecover);
			character.closet.add(Clothing.chestpiece);
			character.closet.add(Clothing.techarmor);
			character.closet.add(Clothing.groincover);
			character.closet.add(Clothing.techpants);
			character.clearSpriteImages();
		}
		if(rank >= 3 && !character.has(Trait.freeenergy)){
			character.add(Trait.freeenergy);
		}
        if(rank >= 2 && !character.has(Trait.smallhands)){
            character.add(Trait.smallhands);
        }
		if(rank >= 1 && !character.has(Trait.madscientist)){
			character.add(Trait.madscientist);
			character.outfit[0].removeAllElements();
			character.outfit[1].removeAllElements();
			character.outfit[0].add(Clothing.bra);
			character.outfit[0].add(Clothing.shirt);
			character.outfit[0].add(Clothing.labcoat);
			character.outfit[1].add(Clothing.underwear);
			character.outfit[1].add(Clothing.pants);
			character.closet.add(Clothing.pants);
			character.closet.add(Clothing.labcoat);
			character.clearSpriteImages();
			character.mod(Attribute.Science, 1);
		}	
	}
	@Override
	public NPC getCharacter() {
		return character;
	}
	
	
	
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case confident: case desperate:
			return value>=30;
		default:
			return value>=50;
		}
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case confident: case desperate:
			return 1.2f;
		default:
			return 1f;
		}
	}
	@Override
	public String image() {
		return "assets/mara_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}
	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Do you like it inside of me? Nice and tight?\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Oooh! Even now... Please, please...\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Mmm, it doesn't matter if you're on top; you can't beat science!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Oh wow! Oh... Harder!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"That's what you get for being such a dirty boy! Cum in my ass already!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Wait! Finish me in any other way!\"</i>");
		comments.put(CommentSituation.ANAL_PITCH_WIN, "<i>\"Hah! I thought men would be ashamed of this, but you seem to like it! Show me how much!\"</i>");
		comments.put(CommentSituation.SELF_BOUND, "<i>\"Pff, I'll have my hands free in just a second, and then I'll use them on you!\"</i>");
		comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Shall I use my hands to finish you off now? You know how good I am with them...\"</i>");
		comments.put(CommentSituation.OTHER_BOUND, "<i>\"Do you like being tied up? I am happy to oblige.\"</i>");
		comments.put(CommentSituation.SIXTYNINE_WIN, "<i>\"Come on! Try a little harder down there!\"</i>");
		comments.put(CommentSituation.SIXTYNINE_LOSE, "<i>\"No fair! I can barely reach your dick. Stop being so tall!\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"Why are you so sexy!? It's not fair!\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"Is my hot body too much for you? Your dick looks ready to burst!\"</i>");
		comments.put(CommentSituation.OTHER_OILED, "<i>\"Your cock is all shiny with oil. It makes me want to play with it!\"</i>");
		comments.put(CommentSituation.PIN_DOM_WIN, "<i>\"You didn't think a little girl could pin you? It's called leverage!\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Mara looks like she just had the wind knocked out of her, and says between gasps; <i>\"Owww!  That was uncalled for, you big bully!\"</i>");
		comments.put(new SkillComment(Attribute.Science,true),"<i>\"How is your multitool shaping up? Let's compare!\"</i>");
		comments.put(new SkillComment(Attribute.Arcane,true),"<i>\"Magic is cheating! They're called the laws of physics for a reason.\"</i>");
		comments.put(new SkillComment(SkillTag.TOY,false),"<i>\"Do you like my new toy?\"</i>");
		comments.put(new SkillComment(SkillTag.TOY,true),"<i>\"Where did you get that? I want one!\"</i>");
		comments.put(new SkillComment(SkillTag.PET,true),"<i>\"Ooh! You brought me a cute friend to play with.\"</i>");
		comments.put(new SkillComment(SkillTag.PET,false),"<i>\"Get him, Slimey!\"</i>");

		return comments;
	}

	@Override
	public CommentGroup getResponses() {
		CommentGroup comments = new CommentGroup();
		return comments;

	}

	@Override
	public int getCostumeSet() {
		if(character.getPure(Attribute.Temporal) > 0){
			return 3;
		}
		if(character.has(Trait.madscientist)){
			return 2;
		}else{
			return 1;
		}
	}
	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if((character.getGrudge()==Trait.inspired || character.getGrudge()==Trait.perfectplan)){
			character.addGrudge(opponent,Trait.planB);
		}else{		
			switch(Global.random(3)){
			case 2:
				if(character.has(Trait.madscientist)) {
					character.addGrudge(opponent,Trait.experimentalweaponry);
					break;
				}
			case 1:
				character.addGrudge(opponent,Trait.inspired);
				break;
			case 0:
				character.addGrudge(opponent,Trait.perfectplan);
				break;
			default:
				break;
			}
		}
		
	}
    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[1].clear();
        if(character.getPure(Attribute.Temporal)>0){
            character.outfit[Character.OUTFITTOP].add(Clothing.nipplecover);
            character.outfit[Character.OUTFITTOP].add(Clothing.chestpiece);
            character.outfit[Character.OUTFITTOP].add(Clothing.techarmor);
            character.outfit[Character.OUTFITBOTTOM].add(Clothing.groincover);
            character.outfit[Character.OUTFITBOTTOM].add(Clothing.techpants);
        }
        else if(character.has(Trait.madscientist)){
            character.outfit[0].add(Clothing.bra);
            character.outfit[0].add(Clothing.shirt);
            character.outfit[0].add(Clothing.labcoat);
            character.outfit[1].add(Clothing.underwear);
            character.outfit[1].add(Clothing.pants);
        }else{
            character.outfit[0].add(Clothing.bra);
            character.outfit[0].add(Clothing.Tshirt);
            character.outfit[1].add(Clothing.underwear);
            character.outfit[1].add(Clothing.shorts);
        }

    }
}
